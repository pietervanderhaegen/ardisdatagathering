import org.nield.kotlinstatistics.*
import pack2d.binsFromXml
import pack2d.common.Bin
import pack2d.readXml
import java.io.File
import java.io.FileWriter
import kotlin.math.abs

fun proccesBinsFile(file: File, report: FileWriter) {
    // read do
    val doc = readXml(file)

    // parse sections
    val bins = binsFromXml(doc)

    //BIN INFO
    val totalBins = getTotalAmountOfBins(bins)
    val binVariation = bins.size.toDouble() / totalBins.toString().toDouble()

    val minDimDescriptives = bins.map { it.minDim }.descriptiveStatistics
    val maxDimDescriptives = bins.map { it.maxDim }.descriptiveStatistics
    val surfaceDescriptives = bins.map { it.surface }.descriptiveStatistics

    report.append(
        "${file.name}, " +
                "${bins.size}, " +
                "${totalBins}, " +
                "${binVariation}, " +

                "${minDimDescriptives.min}, " +
                "${minDimDescriptives.max}, " +
                "${minDimDescriptives.min / minDimDescriptives.max}, " +
                "${minDimDescriptives.sum}, " +
                "${minDimDescriptives.mean}, " +
                "${bins.map { it.minDim }.median()}, " +
                "${minDimDescriptives.max - minDimDescriptives.min}, " +
                "${minDimDescriptives.variance}, " +
                "${minDimDescriptives.standardDeviation}, " +
                "${minDimDescriptives.geometricMean}, " +
                "${minDimDescriptives.sumSquared}, " +
                "${minDimDescriptives.kurtosis}, " +
                "${minDimDescriptives.skewness}," +

                "${maxDimDescriptives.min}, " +
                "${maxDimDescriptives.max}, " +
                "${maxDimDescriptives.min / minDimDescriptives.max}, " +
                "${maxDimDescriptives.sum}, " +
                "${maxDimDescriptives.mean}, " +
                "${bins.map { it.maxDim }.median()}, " +
                "${maxDimDescriptives.max - minDimDescriptives.min}, " +
                "${maxDimDescriptives.variance}, " +
                "${maxDimDescriptives.standardDeviation}, " +
                "${maxDimDescriptives.geometricMean}, " +
                "${maxDimDescriptives.sumSquared}, " +
                "${maxDimDescriptives.kurtosis}, " +
                "${maxDimDescriptives.skewness}," +

                "${surfaceDescriptives.min}, " +
                "${surfaceDescriptives.max}, " +
                "${surfaceDescriptives.min / surfaceDescriptives.max}, " +
                "${surfaceDescriptives.sum}, " +
                "${surfaceDescriptives.mean}, " +
                "${bins.map { it.surface }.median()}, " +
                "${surfaceDescriptives.max - surfaceDescriptives.min}, " +
                "${surfaceDescriptives.variance}, " +
                "${surfaceDescriptives.standardDeviation}, " +
                "${surfaceDescriptives.geometricMean}, " +
                "${surfaceDescriptives.sumSquared}, " +
                "${surfaceDescriptives.kurtosis}, " +
                "${surfaceDescriptives.skewness}," +

                "${minDimDescriptives.min / maxDimDescriptives.min}," +
                "${minDimDescriptives.max / maxDimDescriptives.max}," +
                "${maxDimDescriptives.min - minDimDescriptives.min}," +
                "${maxDimDescriptives.max - minDimDescriptives.max}" +
                "\n "
    );

}

fun getTotalAmountOfBins(bins: List<Bin>): Any {
    return if (bins.map { it.quantity }.contains(Int.MAX_VALUE))
        Int.MAX_VALUE
    else
        abs(bins.map { it.quantity }.sum())
}

fun printBinsToCsv(folder: String) {
    var xmlCounter = 0
    val report = FileWriter("$folder/reports/bins_report.csv")
    report.append(
        "Problem, " +
                "NumberOfDifferentBins, " +
                "TotalAmountOfBins, " +
                "BinVariation, " +

                "MinMinDimBins, " +
                "MaxMinDimBins, " +
                "RatioMinMaxMinDimBins, " +
                "MinDimSumBins, " +
                "AverageMinDimBins, " +
                "MedianMinDimBins, " +
                "RangeMinDimBins, " +
                "VarianceMinDimBins, " +
                "StandardDeviationMinDimBins, " +
                "GeometricMeanMinDimBins, " +
                "SumOfSquaresMinDimBins, " +
                "KurtosisMinDimBins, " +
                "SkewnessMinDimBins, " +

                "MinMaxDimBins, " +
                "MaxMaxDimBins, " +
                "RatioMinMaxMaxDimBins, " +
                "MaxDimSumBins, " +
                "AverageMaxDimBins, " +
                "MedianMaxDimBins, " +
                "RangeMaxDimBins, " +
                "VarianceMaxDimBins, " +
                "StandardDeviationMaxDimBins, " +
                "GeometricMeanMaxDimBins, " +
                "SumOfSquaresMaxDimBins, " +
                "KurtosisMaxDimBins, " +
                "SkewnessMaxDimBins," +

                "MinSurfaceBins, " +
                "MaxSurfaceBins, " +
                "RatioMinMaxSurfaceBins, " +
                "SurfaceSumBins, " +
                "AverageSurfaceBins, " +
                "MedianSurfaceBins, " +
                "RangeSurfaceBins, " +
                "VarianceSurfaceBins, " +
                "StandardDeviationSurfaceBins, " +
                "GeometricMeanSurfaceBins, " +
                "SumOfSquaresSurfaceBins, " +
                "KurtosisSurfaceBins, " +
                "SkewnessSurfaceBins," +

                "minBinRatio," +
                "maxBinRatio," +
                "minBinRange," +
                "maxBinRange" +
                "\n"
    )
    File(folder).walk().forEachIndexed { index, file ->
        if (file.name.matches(Regex(".*\\.xml$", RegexOption.IGNORE_CASE))) {
            proccesBinsFile(file, report)
        }
    }
    report.flush()
    report.close()
}