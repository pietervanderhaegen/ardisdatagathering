package pack2d.common

data class Strip(
    val rect: Rect,
    var cutConstraint: CutConstraint,
    val parent: Strip?,
    val cuts: MutableList<Double> = mutableListOf<Double>(),
    val childs: MutableList<Strip> = mutableListOf()
) {

    val placedParts: MutableList<PlacedPart> = mutableListOf()

    fun cuts(): List<Double> {
        return when (cutConstraint) {
            CutConstraint.VERTICAL -> childs.map { it.rect.width }
            CutConstraint.HORIZONTAL -> childs.map { it.rect.height }
            else -> throw RuntimeException("unexpected ${cutConstraint}")
        }
    }

    fun absoluteCuts(): List<Double> {
        return if (cuts.isNotEmpty()) {
            val start = when (cutConstraint) {
                CutConstraint.VERTICAL -> rect.left
                CutConstraint.HORIZONTAL -> rect.bottom
                else -> throw RuntimeException("unexpected")
            }
            val head = start + cuts.first()
            val tail = cuts.drop(1)
            tail.fold(mutableListOf<Double>(head)) { accum, elem ->
                accum.add(accum.last() + elem + 4.4)
                accum
            }
        } else
            listOf<Double>()
    }

    val level: Int = if (parent != null) parent.level + 1
    else 0

    fun descendants(): List<Strip> =
        childs + childs.flatMap { strip -> strip.descendants() }

    fun allPlacedParts(): List<PlacedPart> =
        placedParts + childs.flatMap { strip -> strip.allPlacedParts() }

    override fun toString() =
        "S:${cutConstraint}:${rect}"
}