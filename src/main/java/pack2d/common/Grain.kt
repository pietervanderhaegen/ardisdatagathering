package pack2d.common

enum class Grain(val rotatable: Boolean) {
    NONE(rotatable = true) {
        override fun rotated() = NONE
    },
    WIDTH(rotatable = false) {
        override fun rotated() = HEIGHT
    },
    HEIGHT(rotatable = false) {
        override fun rotated() = WIDTH
    };

    abstract fun rotated(): Grain
}