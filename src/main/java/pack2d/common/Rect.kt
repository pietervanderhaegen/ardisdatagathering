package pack2d.common

import pack2d.util.round
import kotlin.math.max
import kotlin.math.min

data class Rect(
    var left: Double,
    var bottom: Double,
    val width: Double,
    val height: Double
) {
//    fun rotated() =
//        Rect(left, bottom, height, width)

    val right: Double
        get() = left + width

    val top: Double
        get() = bottom + height

    val surface: Double =
        width * height

    val surfacem2: Double =
        surface / (1000.0 * 1000.0)

    fun fits(otherWidth: Double, otherHeight: Double): Boolean =
        width >= otherWidth &&
                height >= otherHeight

//    fun fitsWithinRect(other: Rect): Boolean =
//        width <= other.width &&
//                height <= other.height

    fun contains(other: Rect): Boolean =
        left <= other.left &&
                right >= other.right &&
                bottom <= other.bottom &&
                top >= other.top

    fun intersects(other: Rect): Boolean =
        min(this.right, other.right) - max(this.left, other.left) > 0 &&
                min(this.top, other.top) - max(this.bottom, other.bottom) > 0

    fun disjunct(rects: List<Rect>) =
        rects.none { it.intersects(this) }

    override fun toString(): String =
        "R([${left.round(2)},${bottom.round(2)}]->[W${width.round(2)},H${height.round(2)})"

    companion object {
        // returns rectangles which are contained inside another rectangle
        fun contained(rects: List<Rect>): List<Rect> =
            rects.filter { rect -> rects.any { rect != it && it.contains(rect) } }
    }
}