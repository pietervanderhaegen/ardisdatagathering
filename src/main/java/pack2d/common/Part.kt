package pack2d.common

import kotlin.math.max
import kotlin.math.min

interface Part {

    val material: String

    val width: Double

    val height: Double

    val grain: Grain

    val minDim: Double
        get() = min(width, height)

    val maxDim: Double
        get() = max(width, height)

    val surface: Double
        get() = width * height
}