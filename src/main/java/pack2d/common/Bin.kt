package pack2d.common

import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.max
import kotlin.math.min

data class Bin(
    val material: String,
    val width: Double,
    val height: Double,
    val sheetThickness: Double,
    val grain: Grain,
    var quantity: Int,
    val isOffcut: Boolean,
    val restMinWidth: Double,
    val restMinHeight: Double,
    val restMinSurface: Double,
    val price: Double,
    val optionalMaster: Bin? = null
) {
    val freeRects: MutableList<FreeRect> = mutableListOf<FreeRect>()
    val placedPartsForNesting: MutableList<PlacedPart> = mutableListOf<PlacedPart>()
    val offcuts: MutableList<FreeRect> = mutableListOf<FreeRect>()
    val scrap: MutableList<FreeRect> = mutableListOf<FreeRect>()

    val rect: Rect = Rect(0.0, 0.0, width, height)
    val strip: Strip = Strip(rect, CutConstraint.FREE, null)
    val freeRect: FreeRect = FreeRect(rect, this, strip)

    val minDim: Double
        get() = min(width, height)

    val maxDim: Double
        get() = max(width, height)

    val master: Bin =
        optionalMaster ?: this

    val surface: Double
        get() = rect.surface

    val surfacem2: Double
        get() = surface / (1000 * 1000)

    fun placedParts(): List<PlacedPart> =
        if (placedPartsForNesting.isNotEmpty())
            placedPartsForNesting
        else
            strip.allPlacedParts()


    fun partsSurface() = placedParts().sumByDouble { it.surface }

    fun partsSurfacem2() = partsSurface() / (1000.0 * 1000.0)

    fun yield(): Double = partsSurface() / rect.surface

    fun isFull(partDemands: List<PartDemand>): Boolean =
        freeRects.none { freeRect ->
            partDemands.any { partDemand ->
                //if (grain == Grain.HEIGHT)
                //    throw RuntimeException("unexpected grain")
                partDemand.fitsWithinRect(freeRect.rect)
            }
        }

    fun normalizedCopy(): Bin =
        when (grain) {
            Grain.NONE ->
                if (width >= height)
                    this.copy(optionalMaster = this)
                else
                    this.copy(width = height, height = width, optionalMaster = this)
            Grain.WIDTH ->
                this.copy(optionalMaster = this)
            Grain.HEIGHT ->
                this.copy(width = height, height = width, grain = Grain.WIDTH, optionalMaster = this)
        }

    fun moreAvailable(): Boolean =
        master.quantity >= 1

    private fun appendOffcut(freeRect: FreeRect): Unit {
        if (freeRect.width >= restMinWidth &&
            freeRect.height >= restMinHeight &&
            freeRect.surface >= restMinSurface
        )
            offcuts.add(freeRect)
        else
            scrap.add(freeRect)
    }

    fun close() {
        while (freeRects.isNotEmpty()) {
            freeRects.sortByDescending { it.surface }
            val offcut = freeRects.removeAt(0)
            appendOffcut(offcut)

            // cut
            freeRects
                .filter { it.rect.intersects(offcut.rect) }
                .forEach { intersectingFreeRect ->
                    freeRects.remove(intersectingFreeRect)
                    intersectingFreeRect
                        .split_for_nesting(offcut.rect)
                        .forEach { freeRects.add(it) }
                }

//            // remove contained rects
//            freeRects
//                .combine(freeRects)
//                .filter { it.first != it.second && it.first.rect.contains(it.second.rect) }
//                .map { it.second }
//                .forEach {
//                    freeRects.remove(it)
//                }
        }
    }

    fun stacks(): Int =
        ceil(quantity / floor(80 / sheetThickness)).toInt()

    fun strips(): List<Strip> =
        strip.descendants()

    fun cycles(): Int =
        strips().size + 1

    fun mergeRectangles(globalFreeRects: MutableList<FreeRect>) {
        loop@ while (true) {
            for (f1 in freeRects)
                for (f2 in freeRects) {
                    val merged = f1.mergesWith(f2)
                    if (merged != null) {
                        print("X")
                        globalFreeRects.remove(f1)
                        globalFreeRects.remove(f2)
                        globalFreeRects.add(merged)
                        freeRects.remove(f1)
                        freeRects.remove(f2)
                        freeRects.add(merged)
                        continue@loop
                    }
                }
            break
        }
    }

//    override fun toString(): String =
//        "B[${width},${height}]x${quantity}"

}
