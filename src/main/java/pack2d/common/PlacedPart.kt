package pack2d.common

data class PlacedPart(
    override val material: String,
    var left: Double,
    var bottom: Double,
    override val width: Double,
    override val height: Double,
    override val grain: Grain = Grain.NONE,
    val partDemand: PartDemand,
    val freeRect: FreeRect,
    var strip: Strip? = null
) : Part {

    val rect = Rect(left, bottom, width, height)

    val bin: Bin
        get() = freeRect.bin

    fun confirm() {
        partDemand.quantity--
    }

    fun level(): Int =
        (strip?.level ?: -1 ) + 1

    companion object  {
        fun ifFits(material: String, left: Double, bottom: Double, width: Double, height: Double, grain: Grain, partDemand: PartDemand, freeRect: FreeRect): PlacedPart? =
            if (freeRect.rect.fits(width, height))
                PlacedPart(material, left, bottom, width, height, grain, partDemand, freeRect)
            else
                null
    }
}