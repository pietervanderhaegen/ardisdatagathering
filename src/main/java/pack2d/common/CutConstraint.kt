package pack2d.common

enum class CutConstraint {
    FREE,
    HORIZONTAL,
    VERTICAL
}
