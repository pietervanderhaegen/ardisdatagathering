package pack2d.common

data class PartDemandKey(
    val material: String,
    val width: Double,
    val height: Double,
    val grain: Grain
) {
    fun partDemand(quantity: Int) =
        PartDemand(material, width, height, grain, quantity)
}

data class PartDemand(
    override val material: String,
    override val width: Double,
    override val height: Double,
    override val grain: Grain = Grain.NONE,
    var quantity: Int = 1
) : Part {

    val rect = Rect(0.0, 0.0, width, height)

    fun key() =
        PartDemandKey(material, width, height, grain)

    fun candidatePlacements(freeRect: FreeRect): List<PlacedPart> =
        when (grain) {
            Grain.NONE -> {
                val listOfNotNull = listOfNotNull(
                    PlacedPart.ifFits(material, freeRect.left, freeRect.bottom, width, height, grain, this, freeRect),
                    PlacedPart.ifFits(material, freeRect.left, freeRect.bottom, height, width, grain, this, freeRect)
                )
                // println("candidates #${listOfNotNull.size}")
                listOfNotNull
            }
            Grain.WIDTH ->
                listOfNotNull(
                    PlacedPart.ifFits(
                        material,
                        freeRect.left,
                        freeRect.bottom,
                        width,
                        height,
                        grain,
                        this,
                        freeRect
                    )
                )
            Grain.HEIGHT ->
                throw RuntimeException("unexpected grain")
        }


    fun normalized() =
        when (grain) {
            Grain.NONE ->
                if (width >= height)
                    this
                else
                    PartDemand(material, height, width, Grain.NONE, quantity)
            Grain.WIDTH -> this
            Grain.HEIGHT -> PartDemand(material, height, width, Grain.WIDTH, quantity)
        }

    fun fitsWithinRect(other: Rect): Boolean =
        when (grain) {
            Grain.NONE -> other.fits(rect.width, rect.height) || other.fits(rect.height, rect.width)
            Grain.WIDTH -> other.fits(rect.width, rect.height)
            Grain.HEIGHT -> throw RuntimeException("fits within should only be called on normalized parts")
        }
}