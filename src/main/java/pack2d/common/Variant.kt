package pack2d.common

enum class Variant {
    STRIP,
    GUILLOTINE,
    NESTING;
}