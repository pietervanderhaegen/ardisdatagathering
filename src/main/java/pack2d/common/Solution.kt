package main.java.pack2d.common

import pack2d.algorithms.Solver2D
import pack2d.common.Bin
import pack2d.common.PlacedPart
import pack2d.common.Rect
import pack2d.util.round

data class Solution(
    val bins: List<Bin>,
    val solver2D: Solver2D,
    val time: Long,
    val remarks: MutableList<String> = mutableListOf()
) {

    fun binsCount() = bins.sumBy { it.quantity }

    fun binsSurfacem2() = bins.sumByDouble { it.surfacem2 * it.quantity }

    fun partsSurfacem2() = bins.sumByDouble { it.partsSurfacem2() * it.quantity }

    fun yield(): Double = partsSurfacem2() / binsSurfacem2()

    // TODO improve
    fun parts(): List<PlacedPart> =
        bins.flatMap { bin ->
            (1..bin.quantity).flatMap { bin.placedParts() }
        }

    fun stacks(): Int =
        bins.sumBy { it.stacks() }

    fun cycles(): Int =
        bins.sumBy { it.cycles() }

    fun usedOffcuts(): List<Rect> =
        bins.filter { it.isOffcut }.map { it.rect }

    fun createdOffcuts(): List<Rect> =
        bins.flatMap { it.offcuts }.map { it.rect }

    fun cost(): Double = costMaterial() + costStacks() + costCycles() + costRests()

    fun costCycles() =
        cycles() * 2.0

    fun costStacks() =
        stacks() * 4.0

    fun costMaterial() =
        binsSurfacem2() * 20.0

    fun costRests(): Double =
        costUsedRests() - costCreatedRests()

    val offcutCost: (Double) -> Double =
        { x -> 0.5 * x * x + 0.5483 * x }

    fun costUsedRests(): Double =
        usedOffcuts().map { offcutCost(it.surfacem2) }.sum()

    fun costCreatedRests(): Double =
        createdOffcuts().map { offcutCost(it.surfacem2) }.sum()

    fun print(): Unit {
        println("placed parts ${parts().size}")
        println("cost ${cost()}")
        println("costMaterial ${costMaterial()}")
        println("costStacks ${costStacks()} for ${stacks()} stacks with height ${bins.first().sheetThickness}")
        println("costCreatedRests ${costCreatedRests()}")
        println("costUsedRests ${costUsedRests()}")
        println("patterns ${bins.size}")
        println("bins ${binsCount()}")
        println("yld ${yield()}")
    }

    fun summary(): String =
        "parts ${parts().size} - cost ${cost().round(2)} - patterns ${bins.size} - bins ${binsCount()} ${binsSurfacem2().round(
            2
        )} - ${solver2D}"

}