package pack2d.common

data class FreeRect(val rect: Rect, val bin: Bin, val strip: Strip? = null) {

    val left: Double
        get() = rect.left

    val right: Double
        get() = rect.right

    val bottom: Double
        get() = rect.bottom

    val top: Double
        get() = rect.top

    val width: Double
        get() = rect.width

    val height: Double
        get() = rect.height

    val surface: Double
        get() = rect.surface

    fun intersects(placedPart: PlacedPart): Boolean =
        bin == placedPart.bin &&
                rect.intersects(placedPart.rect)

    ////////////////////////
    // f2     //  f1      //
    ////////////          //
    //XXXXXXXX//          //
    //XXXXXXXX//          //
    //XXXXXXXX//          //
    ////////////////////////
    fun verticalCut(placedPart: PlacedPart): List<FreeRect> {
        if (strip != null) {
            when (strip.cutConstraint) {
                CutConstraint.FREE -> {
                    if (placedPart.width < width)
                        strip.cutConstraint = CutConstraint.VERTICAL
                    else if (placedPart.height < height)
                            return horizontalCut(placedPart)
                         else
                            strip.cutConstraint = CutConstraint.VERTICAL
                }
                CutConstraint.VERTICAL -> { }
                CutConstraint.HORIZONTAL -> throw RuntimeException("unexpected cut constraint")
            }
        }
        else
            bin.placedPartsForNesting.add(placedPart)
        val f1 = run {
            // println("VC:f1:${placedPart.rect}")
            val left = rect.left + placedPart.width + sawMargin
            val bottom = rect.bottom
            val width = rect.width - placedPart.width - sawMargin
            val height = rect.height
            if (width > 0 && height > 0) {
                if (strip != null)
                    strip.cuts.add(placedPart.width)
                FreeRect(Rect(left, bottom, width, height), bin, strip)
            } else {
                null
            }
        }
        val f2 = run {
            // println("VC:f2:${placedPart.rect}")
            val left = rect.left
            val bottom = rect.bottom + placedPart.height + sawMargin
            val width = placedPart.width
            val height = rect.height - placedPart.height - sawMargin
            if (width > 0 && height > 0) {
                val newFreeRectRect: Rect = Rect(left, bottom, width, height)
                // println("VC: new free rect: ${newFreeRectRect} placing ${placedPart.rect}")
                if (strip != null) {
                    val newStrip =  Strip(Rect(rect.left, rect.bottom, placedPart.width, rect.height), CutConstraint.HORIZONTAL, strip)
                    newStrip.placedParts.add(placedPart)
                    placedPart.strip = newStrip
                    // println("VC: new new L${newStrip.level} strip with element: ${newStrip.rect} with placed ${placedPart.rect}")
                    newStrip.cuts.add(placedPart.height)
                    strip.childs.add(newStrip)
                    FreeRect(newFreeRectRect, bin, newStrip)
                } else FreeRect(newFreeRectRect, bin)
            } else {
                if (strip != null) {
                    // println("V:f2/2 L${strip?.level} placed ${placedPart.rect}:${Rect(left, bottom, width, height)}")
                    strip.placedParts.add(placedPart)
                    placedPart.strip = strip
                }
                null
            }
        }
        // println("f1:${f1?.rect}, f2:${f2?.rect}")
        return listOfNotNull(f1, f2)
    }

    ////////////////////////
    //  f1                //
    ////////////////////////
    //XXXXXXXX//  f2      //
    //XXXXXXXX//          //
    //XXXXXXXX//          //
    ////////////////////////
    fun horizontalCut(placedPart: PlacedPart): List<FreeRect> {
        if (strip != null) {
            when (strip.cutConstraint) {
                CutConstraint.FREE -> {
                    if (placedPart.height < height)
                        strip.cutConstraint = CutConstraint.HORIZONTAL
                    else if (placedPart.width < width)
                            return verticalCut(placedPart)
                         else
                            strip.cutConstraint = CutConstraint.HORIZONTAL
                }
                CutConstraint.VERTICAL -> throw RuntimeException("unexpected cut constraint")
                CutConstraint.HORIZONTAL -> {
                }
            }
        }
        else
            bin.placedPartsForNesting.add(placedPart)
        val f1 = run {
            // println("HC:f1:${placedPart.rect}")
            val left = rect.left
            val bottom = rect.bottom + placedPart.height + sawMargin
            val width = rect.width
            val height = rect.height - placedPart.height - sawMargin
            if (width > 0 && height > 0) {
                if (strip != null)
                    strip.cuts.add(placedPart.height)
                FreeRect(Rect(left, bottom, width, height), bin, strip)
            } else
                null
        }
        val f2 = run {
            // println("HC:f2:${placedPart.rect}")
            val left = rect.left + placedPart.width + sawMargin
            val bottom = rect.bottom
            val width = rect.width - placedPart.width - sawMargin
            val height = placedPart.height
            if (width > 0 && height > 0) {
                val newFreeRectRect: Rect = Rect(left, bottom, width, height)
                // println("HC: new free rect: ${newFreeRectRect}")
                if (strip != null) {
                    val newStrip = Strip(Rect(rect.left, rect.bottom, rect.width, placedPart.height), CutConstraint.VERTICAL, strip)
                    newStrip.placedParts.add(placedPart)
                    placedPart.strip = newStrip
                    // println("HC: new L${newStrip.level} strip with element: ${newStrip.rect} with placed ${placedPart.rect}")
                    newStrip.cuts.add(placedPart.height)
                    strip.childs.add(newStrip)
                    FreeRect(newFreeRectRect, bin, newStrip)
                } else FreeRect(newFreeRectRect, bin)
            } else {
                if (strip != null) {
                    // println("H:f2/2 L${strip?.level} placed ${placedPart.rect}:${Rect(left, bottom, width, height)}")
                    strip.placedParts.add(placedPart)
                    placedPart.strip = strip
                }
                null
            }
        }
        // println("f1:${f1?.strip?.cutConstraint}${f1?.rect}, f2:${f2?.strip?.cutConstraint}${f2?.rect}")
        return listOfNotNull(f1, f2)
    }

    fun mergesWith(other: FreeRect): FreeRect? =
        when {
            // |-------|
            // |       |
            // |-------|
            // |-------|
            // | other |
            // |-------|
            left == other.left && width == other.width && bottom == other.top + sawMargin -> this.copy(
                rect = Rect(
                    left,
                    other.bottom,
                    width,
                    other.height + sawMargin + height
                )
            )
            // |-------|
            // | other |
            // |-------|
            // |-------|
            // |       |
            // |-------|
            left == other.left && width == other.width && top + sawMargin == other.bottom -> this.copy(
                rect = Rect(
                    left,
                    bottom,
                    width,
                    height + sawMargin + other.height
                )
            )
            // |-------||-------|
            // |       || other |
            // |-------||-------|
            bottom == other.bottom && height == other.height && right + sawMargin == other.left -> this.copy(
                rect = Rect(
                    left,
                    bottom,
                    width + sawMargin + other.width,
                    height
                )
            )
            // |-------||-------|
            // | other ||       |
            // |-------||-------|
            bottom == other.bottom && height == other.height && other.right + sawMargin == left -> this.copy(
                rect = Rect(
                    other.left,
                    other.bottom,
                    other.width + sawMargin + width,
                    height
                )
            )
            else -> null
        }

    //    ----------------------------------------------
    //    |                 f2                         |
    //    |              |------|                      |
    //    |      f1      |      |         f3           |
    //    |              |  PP  |                      |
    //    |--------------------------------------------|
    //                   |      |
    //                   |------|
    //                      f4
    fun split_for_nesting(rect: Rect): List<FreeRect> {
        assert(rect.intersects(rect)) { "expecting rects to overlap" }
        // f1
        val f1: FreeRect? = run {
            val width = rect.left - left - sawMargin
            if (width > 0)
                this.copy(rect = Rect(left, bottom, width, height))
            else
                null
        }

        // f2
        val f2: FreeRect? = run {
            val height = top - rect.top - sawMargin
            if (height > 0)
                this.copy(rect = Rect(left, rect.top + sawMargin, width, height))
            else
                null
        }

        // f3
        val f3: FreeRect? = run {
            val width = right - rect.right - sawMargin
            if (width > 0)
                this.copy(rect = Rect(rect.right + sawMargin, bottom, width, height))
            else
                null
        }

        // f4
        val f4: FreeRect? = run {
            val height = rect.bottom - bottom - sawMargin
            if (height > 0)
                this.copy(rect = Rect(left, bottom, width, height))
            else
                null
        }

        return listOfNotNull(f1, f2, f3, f4)
    }

    companion object {
        const val sawMargin = 4.4
    }
}
