package pack2d.util

fun <T1, T2> Collection<T1>.combine(other: Iterable<T2>): List<Pair<T1, T2>> =
    combine(other, { thisItem: T1, otherItem: T2 -> Pair(thisItem, otherItem) })

fun <T1, T2, R> Collection<T1>.combine(other: Iterable<T2>, combiner: (T1, T2) -> R): List<R> =
    this.flatMap { firstItem -> other.map { secondItem -> combiner(firstItem, secondItem) } }

fun <T1, T2, R> Collection<T1>.combineFlat(other: Iterable<T2>, combiner: (T1, T2) -> List<R>): List<R> =
    this.flatMap { firstItem -> other.flatMap { secondItem -> combiner(firstItem, secondItem) } }
