package pack2d.util

import java.math.BigDecimal
import java.math.RoundingMode

fun Double.round(decimals: Int): BigDecimal =
    BigDecimal(this).setScale(decimals, RoundingMode.HALF_EVEN)