package pack2d.util

import pack2d.common.*
import java.awt.*
import java.awt.geom.AffineTransform
import java.awt.image.BufferedImage
import java.io.File
import kotlin.math.roundToInt

object ExportPNG {
    fun exportBins(folder: String, mat: String, bins: List<Bin>): Unit {
        try {
            File(folder).mkdir()
        } catch (e: Throwable) {
            println("folder exists")
        }
        try {
            File("${folder}/${mat}").mkdir()
        } catch (e: Throwable) {
            println("folder exists")
        }
        try {
            File("${folder}/${mat}/").listFiles().filterNotNull().forEach { it.delete() }
        } catch (e: Throwable) {
            println("no files to delete")
        }
        // export bins
        for ((index, bin) in bins.withIndex()) {
            exportBin("${folder}/${mat}/", bin, index)
        }
        println()
    }

    private val dashedStroke =
        BasicStroke(10.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, floatArrayOf(10.0f), 0.0f)

    fun exportBin(folder: String, bin: Bin, index: Int): Unit {
        if (bin.placedParts().isEmpty())
            return
        // create an image
        val canvas = BufferedImage(bin.width.toInt() + 50, bin.height.toInt() + 50, BufferedImage.TYPE_INT_RGB)

        // get Graphics2D for the image
        val g = canvas.createGraphics()

        // clear background
        g.stroke = BasicStroke(1.0f)
        g.color = Color.DARK_GRAY
        g.fillRect(0, 0, bin.width.toInt() + 50, bin.height.toInt() + 50)
        g.color = Color.WHITE
        g.fillRect(25, 25, bin.width.toInt(), bin.height.toInt())

        val largeFont: Font = g.font.deriveFont(72.0f)
        val regularFont: Font = g.font.deriveFont(36.0f)
        val affineTransform: AffineTransform = AffineTransform()
        affineTransform.rotate(Math.toRadians(90.0), 0.0, 0.0)
        val rotatedFont: Font = regularFont.deriveFont(affineTransform).deriveFont(36.0f)

//        val cutFont: Font = regularFont.deriveFont(affineTransform).deriveFont(48.0f)
//        val defaultStroke = g.stroke
//        val halfSawMargin = (FreeRect.sawMargin / 2).toInt()

        for (part in bin.placedParts())
            drawPart(g, part, rotatedFont, regularFont)

        for (scrap in bin.scrap)
            drawScrap(g, scrap, rotatedFont, regularFont)

        for (rest in bin.offcuts)
            drawRest(g, rest, rotatedFont, regularFont)

        for (freeRect in bin.freeRects)
            drawFreeRect(g, freeRect, rotatedFont, regularFont)

        for (strip in bin.strips().filter { it.level != 0 })
            drawStrip(g, strip)

        g.setFont(largeFont)
        g.color = Color.BLUE
        g.stroke = BasicStroke(8.0f)
        drawCenteredString(
            g,
            "${bin.rect.width} x ${bin.rect.height} # ${bin.quantity}",
            Rectangle(0, 0, 50 + bin.width.toInt(), 50 + bin.height.toInt()),
            largeFont
        )

        // done with drawing
        g.dispose()

        // write image to a file
        val indexWithLeadingZero = "${index}".padStart(3, '0')
        val fileName = "${folder}/${indexWithLeadingZero}_${(bin.yield() * 100.0).roundToInt()}_${bin.material.replace(
            Regex("[^a-zA-Z0-9\\.\\-]"),
            "_"
        )}.png"
//        print(".")
        javax.imageio.ImageIO.write(canvas, "png", File(fileName))
    }

    ////////////

    fun drawFillAtPos(
        g: Graphics2D,
        r: Rect,
        fill: Color
    ) {
        g.color = fill
        g.fillRect(r.left.toInt() + 25 + 8, r.bottom.toInt() + 25 + 8, r.width.toInt() - 8, r.height.toInt() - 8)
    }

    /**
     * Draw a String centered in the middle of a Rectangle.
     *
     * @param g The Graphics instance.
     * @param text The String to draw.
     * @param rect The Rectangle to center the pack2d.text in.
     */
    fun drawCenteredString(g: Graphics, text: String, rect: Rectangle, font: Font) {
        // Get the FontMetrics
        val metrics = g.getFontMetrics(font)
        // Determine the X coordinate for the pack2d.text
        val x = rect.x + (rect.width - metrics.stringWidth(text)) / 2
        // Determine the Y coordinate for the pack2d.text (note we add the ascent, as in java 2d 0 is top of the screen)
        val y = rect.y + (rect.height - metrics.height) / 2 + metrics.ascent
        // Set the font
        g.font = font
        // Draw the String
        g.drawString(text, x, y)
    }

    fun drawRectAtPos(
        g: Graphics2D,
        r: Rect,
        fill: Color,
        paint: Paint?,
        border: Color,
        label: String,
        rotatedFont: Font,
        regularFont: Font
    ): Unit {
        g.color = fill
        if (paint != null) g.paint = paint
        g.fillRect(r.left.toInt() + 25 + 8, r.bottom.toInt() + 25 + 8, r.width.toInt() - 8, r.height.toInt() - 8)
        g.color = border
        g.stroke = BasicStroke(4.0f)
        g.drawRect(r.left.toInt() + 25 + 4, r.bottom.toInt() + 25 + 4, r.width.toInt() - 4, r.height.toInt() - 4)
        drawCenteredString(
            g,
            "${r.width.round(2)}",
            Rectangle(r.left.toInt(), r.bottom.toInt(), r.width.toInt(), 25 + 36 * 2),
            regularFont
        )
        drawCenteredString(
            g,
            "${r.height.round(2)}",
            Rectangle(r.left.toInt(), r.bottom.toInt(), 25 + 36 * 2, r.height.toInt()),
            rotatedFont
        )
        drawCenteredString(
            g,
            label,
            Rectangle(r.left.toInt(), r.bottom.toInt(), r.width.toInt(), r.height.toInt()),
            regularFont
        )
    }

    private fun drawRest(g: Graphics2D, f: FreeRect, rotatedFont: Font, regularFont: Font): Unit {
        drawRectAtPos(
            g,
            f.rect,
            Color(128, 255, 128, 32),
            null,
            Color(128, 255, 128, 196).darker(),
            "",
            rotatedFont,
            regularFont
        )
    }

    private fun drawStrip(g: Graphics2D, s: Strip): Unit {
        drawFillAtPos(
            g,
            Rect(s.rect.left + 30, s.rect.bottom + 30, s.rect.width - 60, s.rect.height - 60),
            Color(0, 0, 255, 16)
        )
    }

    private fun drawScrap(g: Graphics2D, f: FreeRect, rotatedFont: Font, regularFont: Font): Unit {
        drawRectAtPos(
            g,
            f.rect,
            Color(255, 128, 128, 32),
            null,
            Color(255, 128, 128, 196).darker(),
            "",
            rotatedFont,
            regularFont
        )
    }

    private fun drawPart(g: Graphics2D, p: PlacedPart, rotatedFont: Font, regularFont: Font): Unit {
        drawRectAtPos(
            g,
            p.rect,
            Color(64, 64, 64, 32),
            null,
            Color.BLACK,
            "",
            rotatedFont,
            regularFont
        )
    }

    private fun drawFreeRect(g: Graphics2D, f: FreeRect, rotatedFont: Font, regularFont: Font): Unit {
        drawRectAtPos(
            g,
            f.rect,
            Color(128, 128, 255, 0),
            null,
            Color.RED,
            "F",
            rotatedFont,
            regularFont
        )
    }
}