package pack2d.util

import main.java.pack2d.common.Solution
import pack2d.common.*
import java.io.File

data class ExportSVG(val filename: String, val solution: Solution) {
    fun html(): String =
        """
        <html>
          <style>
            .container {
              margin-left: auto;
              margin-right: auto;
              max-width: 1024px;
              width: 1024px;
            }
            .sheet, .strip, .part {
              fill: rgb(128,128,128);
              stroke: rgb(0,0,0);
            }
            .sheet {
              fill: rgba(128,128,128,0);
              stroke-width: 32px;
              stroke-dasharray: 10,0;
              stroke: rgb(128,0,0);
              opacity: 1;
            }
            .strip {
              fill: rgb(64,64,64);
              stroke: rgb(255,0,0);
              stroke-width: 0px;
              opacity: 0.2;
            }
            .part {
              fill:rgb(64,128,64);
              stroke: rgb(0,64,0);
              stroke-width: 6px;
              opacity: 0.5;
            }
            .dashed {
              stroke-dasharray: 10,10;
            }
            .cut {
              stroke: rgb(255,0,0);
              stroke-width: 16px;
              opacity: 1;
            }
            .label {
              fill: black;
              font-size: ${fontSize()}px;
            }
            .partList {
              // width: 100%;
            }
            .partList td, .partList th {
              pack2d.text-align: right;
            }
            .rest {
              // fill: rgb(235,255,235);
              opacity: 0.75;
              stroke: rgb(0,0,0);
              stroke-width: 6px; 
            }
            .level_0 {
                fill: grey;
            }
            .level_1 {
                fill: green;
            }
            .level_2 {
                fill: blue;
            }
            .level_3 {
                fill: yellow;
            }
            .level_4 {
                fill: orange;
            }
            .level_5 {
                fill: red;
            }
            .level_5 {
                fil6: purple;
            }
          </style>
          <body>
            <div class="container">
            <h1>${filename}</h1>
              <dl>
                <dt>Duration</dt>
                <dd>${solution.time}ms</dd>
                <dt>Parts #</dt>
                <dd>placed ${solution.parts().size} from ${solution.solver2D.parts.map{ it.quantity }.sum()}</dd>
                <dt>Layouts # / Bins #</dt>
                <dd>${solution.bins.size} / ${solution.binsCount()}</dd>
                <dt>Yield %</dt>
                <dd>${(solution.yield()*100).round(2)} : ${solution.partsSurfacem2().round(2)} / ${solution.binsSurfacem2().round(2)}</dd>
                <dt>Stacks / Cycles</dt>
                <dd>${solution.stacks()} / ${solution.cycles()}</dd>
                <dt>Cost</dt>
                <dd>${solution.cost().round(2)}</dd>
                <dt>Costs</dt>
                <dd>M: ${solution.costMaterial().round(3)} + S:${solution.costStacks()} + C:${solution.costCycles()} + R:${solution.costRests().round(3)}</dd>
                <dt>Solver</dt>
                <dd>${solution.solver2D}</dd>
              </dl>
            ${sheets()}
            <hr/>
            ${partsList()}
            </div>
          </body>
        </html>
        """.trimIndent()


    fun fontSize(): Int {
        // val avgDim = solution.parts().map { it.minDim + it.maxDim }.sum() / 2 / solution.parts().size
        // return (avgDim / 8.0).toInt()
        return 64
    }

    fun partsList(): String =
        """<h1>Part List (#${solution.solver2D.parts.map{ it.quantity }.sum()})</h1>
           <table class="partList">
             <tr><th>QTY</th><th>Width</th><th>Height</th><th>Grain</th></tr> 
             ${solution.solver2D.parts.sortedWith(compareBy({ it.normalized().width }, { it.normalized().height })).map { partsListRow(it) }.joinToString(separator = "\n")}
           </table>""".trimIndent()

    fun partsListRow(part: PartDemand) =
        """<tr><td>${part.quantity}</td><td>${part.normalized().width}</td><td>${part.normalized().height}</td><td>${part.normalized().grain}</td></tr>
        """.trimIndent()

    fun rests(rests: List<FreeRect>) =
        rests.map { rest(it) }.joinToString(separator = "\n")

    fun rest(rest: FreeRect): String {
        val xr: Double = rest.left.round(2).toDouble()
        val xt: Double = (rest.left + rest.width / 2).round(0).toDouble()
        val yr: Double = rest.bottom.round(2).toDouble()
        val yt: Double = (rest.bottom + rest.height / 2).round(0).toDouble()
        val width: Double = rest.width.round(2).toDouble()
        val height: Double = rest.height.round(2).toDouble()
        return """<rect x="${xr}" y="${yr}" width="${width}" height="${height}" class="rest" fill="url(#diag)" />
                  <pack2d.text x="${xr+fontSize()/2}" y="${yt}" dominant-baseline="middle" pack2d.text-anchor="middle" style="writing-mode: tb;" class="label">${height}</pack2d.text>
                  <pack2d.text x="${xt}" y="${yr+fontSize()*3/4}" dominant-baseline="middle" pack2d.text-anchor="middle" class="label">${width}</pack2d.text>
                  """.trimIndent()
    }

    fun parts(parts: List<PlacedPart>) =
        parts.map { part(it) }.joinToString(separator = "\n")

    fun part(part: PlacedPart): String {
        val xr: Double = part.left.round(2).toDouble()
        val xt: Double = (part.left + part.width / 2).round(0).toDouble()
        val yr: Double = part.bottom.round(2).toDouble()
        val yt: Double = (part.bottom + part.height / 2).round(0).toDouble()
        val width: Double = part.width.round(2).toDouble()
        val height: Double = part.height.round(2).toDouble()
        return """<rect x="${xr}" y="${yr}" width="${width}" height="${height}" class="part level_${part.level()}"/>
                  <pack2d.text x="${xr+fontSize()/2}" y="${yt}" dominant-baseline="middle" pack2d.text-anchor="middle" style="writing-mode: tb;" class="label">${height}</pack2d.text>
                  <pack2d.text x="${xt}" y="${yr+fontSize()*3/4}" dominant-baseline="middle" pack2d.text-anchor="middle" class="label">${width}</pack2d.text>
                  """.trimIndent()
    }

//    fun strips(strips: List<Strip>) =
//        strips.map { strip(it) }.joinToString(separator = "\n")

    fun strip(strip: Strip): String {
        val margin = 20
        val xr = strip.rect.left + margin
        val yr = strip.rect.bottom + margin
        val width = strip.rect.width - margin * 2
        val height = strip.rect.height - margin * 2
        return """
                <rect x="${xr}" y="${yr}" width="${width}" height="${height}" class="strip"/>
                ${strip.absoluteCuts().map { stripCut(strip.rect, strip.cutConstraint, it) }.joinToString(separator = "\n")}
               """.trimIndent()
    }

    fun stripCut(rect: Rect, cutConstraint: CutConstraint, cut: Double): String {
        return if (cutConstraint == CutConstraint.VERTICAL) {
            val x1 = rect.left + cut
            val y1 = rect.bottom
            val x2 = x1
            val y2 = rect.top
            """<line x1="${x1}" y1="${y1}" x2="${x2}" y2="${y2}" class="cut"/>""".trimIndent()
        } else {
            val x1 = rect.left
            val y1 = rect.bottom + cut
            val x2 = rect.right
            val y2 = y1
            """<line x1="${x1}" y1="${y1}" x2="${x2}" y2="${y2}" class="cut"/>""".trimIndent()
        }
    }

    fun sheets() =
        solution.bins.map { sheet(it) }.joinToString(separator = "\n")

    fun sheet(bin: Bin): String {
        val margin = 16
        val width = bin.width + 2 * margin
        val height = bin.height + 2 * margin
        return """
               <h2>${bin.freeRect.rect.width} x ${bin.freeRect.rect.height}</h2>
               <dl>
                 <dt>Quantity</dt>
                 <dd>${bin.quantity}</dd>
                 <dt>Grain</dt>
                 <dd>${bin.grain}</dd>
                 <dt>Yield</dt>
                 <dd>${(bin.yield()*100).round(2)} : ${bin.partsSurfacem2().round(2)} / ${bin.surfacem2.round(2)}</dd>
                 <dt>Stacks / Cycles</dt>
                 <dd>${bin.stacks()} / ${bin.cycles()}</dd>
                 <dt>Main Strips</dt>
                 <dd>${bin.strip.cuts} / ${bin.strip.cuts.sum()}</dd>
                 <dt>Main Strips</dt>
                 <dd>${bin.strip.cuts()} / ${bin.strip.cuts().sum()}</dd>
               </dl>  
               <svg viewBox="${-margin} ${-margin} ${width} ${height}" width="100%">
                <defs>
                    <pattern id="diag" patternUnits="userSpaceOnUse" width="16" height="16" patternTransform="rotate(45)">
                        <line x1="0" y="0" x2="0" y2="16" stroke="#C3E2D3" stroke-width="16" />
                    </pattern>
                </defs>               
                 ${parts(bin.placedParts())}
                 ${rests(bin.offcuts)} 
                 ${strip(bin.strip)}
                 <rect x="${-margin}" y="${-margin}" width="${width}" height="${height}" class="sheet" />
               </svg> 
               """.trimIndent()
    }

    companion object {
        fun exportBins(filename: String, solution: Solution): Unit {
            File(filename).writeText(ExportSVG(File(filename).nameWithoutExtension, solution).html())
        }
    }
}