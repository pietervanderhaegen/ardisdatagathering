package pack2d.algorithms

import main.java.pack2d.common.Solution
import pack2d.algorithms.strategy.BinSorter
import pack2d.algorithms.strategy.FreeRectSplitter
import pack2d.algorithms.strategy.PartSorter
import pack2d.algorithms.strategy.PartToRectMatcher
import pack2d.common.*
import pack2d.util.combine
import pack2d.util.combineFlat
import kotlin.system.measureTimeMillis

// best overall results
// GUILLOTINE-BSSF-SAS-RM-DESCSS-BFF

class Solver2D(
    val bins: List<Bin>,
    val parts: List<PartDemand>,
    val globalFreeRectangleSearch: Boolean = false,
    val copyBinPattern: Boolean = true,
    val partSorter: PartSorter = PartSorter.DescendingShorterSide,
    val binSorter: BinSorter = BinSorter.DescendingSurface,
    val partToRectMatcher: PartToRectMatcher = PartToRectMatcher.BestShortestSideFit,
    val freeRectSplitter: FreeRectSplitter = FreeRectSplitter.ShortestAxisSplit,
    val variant: Variant
) {


    val availablePartDemands: MutableList<PartDemand> = partSorter.sort(parts)

    private var minDemandWidth: Double = parts.map { it.width }.min() ?: 0.0
    private var minDemandHeight: Double = parts.map { it.height }.min() ?: 0.0

    // sorting of the bins
    val availableBins = binSorter.sort(bins)

    // current open bins
    val usedBins = mutableListOf<Bin>()

    // list of free rectangles
    val freeRects = mutableListOf<FreeRect>()
    val freeRectsScrap = mutableListOf<FreeRect>()

    fun openBin(bin: Bin): Bin {
        // println("opening bin ${bin}")
        if (--bin.quantity == 0) availableBins.remove(bin)
        val openBin = bin.copy(quantity = 1, optionalMaster = bin)
        usedBins.add(openBin)
        return openBin
    }

    fun openBinAndFindPartPlaced(consideredPartDemands: List<PartDemand>): PlacedPart? {
        val binMaster = availableBins.find { bin ->
            consideredPartDemands.any { partDemand -> partDemand.fitsWithinRect(bin.rect) }
        }
        return if (binMaster != null) {
            val bin = openBin(binMaster)
            val freeRect = bin.freeRect
            freeRects.add(freeRect)
            findPartPlaced(consideredPartDemands)
        } else
            null
    }

    fun findPartPlaced(partDemands: List<PartDemand>): PlacedPart? =
        partDemands
            .combineFlat(freeRects) { part, freeRect -> part.candidatePlacements(freeRect) }
            // .sortedWith(compareBy({ it.freeRect.bottom }, { it.freeRect.left }, { it.freeRect.surface }))
            .minBy {
                val fitness = partToRectMatcher.fitness(it.rect, it.freeRect.rect)
                // println("fitness: ${fitness} for ${it.rect} ${it.freeRect.rect} ${this}")
                fitness
            }

    fun copyBinPattern(bin: Bin): Unit = run {
        // assert(bin.isFull(availablePartDemands)) { "the bin should be full before it's copied" }
        while (
            bin.master.moreAvailable() &&
            sufficientParts(bin.placedParts())
        ) {
            bin.quantity++
            if (--bin.master.quantity == 0)
                availableBins.remove(bin.master)
            for (part in bin.placedParts()) {
                part.partDemand.quantity--
                if (part.partDemand.quantity == 0)
                    availablePartDemands.remove(part.partDemand)
            }
        }
    }

    fun totalSurface() =
        usedBins.sumByDouble { it.surfacem2 * it.quantity }

    fun solve(maxSurface: Double): Solution? {
        // println(availablePartDemands.map { it.surface }.joinToString(", "))
        val time = measureTimeMillis {
            while (availablePartDemands.isNotEmpty() && totalSurface() < maxSurface) {
                // limit the part demands to consider
                val consideredPartDemands =
                    if (globalFreeRectangleSearch)
                        availablePartDemands
                    else
                        availablePartDemands.take(1)

                // find a free rect, or open a new bin, or return if no free rectangle can be found
//                println("considered part demands #${consideredPartDemands.size}")
//                println("free rects ${freeRects.size}")
                val placedPart: PlacedPart =
                    findPartPlaced(consideredPartDemands) ?: openBinAndFindPartPlaced(consideredPartDemands) ?: break

                // the available quantity should be strictly positive
                // assert(partPlaced.partDemand.quantity > 0) { "unexpected master quantity" }

                // place the part in the free rectangle
                placedPart.confirm()

                // remove the free rectangle from the global list of free rectangles
                freeRects.remove(placedPart.freeRect)
                // remove the free rectangle from the bin list of free rectangles
                placedPart.freeRect.bin.freeRects.remove(placedPart.freeRect)

                // remove the part (and it's rotation if applicable) from the available parts when there no more demand
                // println("${partDemand.quantity} x ${partDemand}")
                if (placedPart.partDemand.quantity == 0) {
                    availablePartDemands.remove(placedPart.partDemand)

                    // update the cached minimum dimensions
                    if (placedPart.partDemand.width == minDemandWidth)
                        minDemandWidth = availablePartDemands.map { it.width }.min() ?: Double.MAX_VALUE
                    if (placedPart.partDemand.height == minDemandHeight)
                        minDemandHeight = availablePartDemands.map { it.height }.min() ?: Double.MAX_VALUE
                }

                val newFreeRects = freeRectSplitter.split(placedPart.freeRect, placedPart)

                // add the new free rectangles to the list of free rectangles
                // (they shouldn't overlap with the placed part)
                // in the non nesting case we can optimize this because we can discard small free rectangles immediately
                for (newFreeRect in newFreeRects) {
                    if (newFreeRect.rect.fits(minDemandWidth, minDemandHeight) ||
                        newFreeRect.rect.fits(minDemandHeight, minDemandWidth) ||
                        variant == Variant.NESTING
                    )
                        freeRects.add(newFreeRect)
                    else
                        freeRectsScrap.add(newFreeRect)
                    newFreeRect.bin.freeRects.add(newFreeRect)
                }

//                if (variant == Variant.STRIP) {
//                    // remove overlapping free rects
//                    // for the flex strip case
//                    partPlaced
//                        .bin
//                        .freeRects
//                        .filter { it.rect.intersects(partPlaced.rect) }
//                        .forEach { intersectingFreeRect ->
//                            // remove
//                            freeRects.remove(intersectingFreeRect)
//                            intersectingFreeRect.bin.freeRects.remove(intersectingFreeRect)
//                        }
//                }

                // when nesting, we need to cut up overlapping free rectangles
                if (variant == Variant.NESTING) {
                    placedPart
                        .bin
                        .freeRects
                        .filter { it.rect.intersects(placedPart.rect) }
                        .forEach { intersectingFreeRect ->
                            // remove
                            freeRects.remove(intersectingFreeRect)
                            intersectingFreeRect.bin.freeRects.remove(intersectingFreeRect)
                            assert(intersectingFreeRect.bin == placedPart.bin) { "expecting bins to be equal" }
                            // split
                            intersectingFreeRect
                                .split_for_nesting(placedPart.rect)
                                .forEach {
                                    freeRects.add(it)
                                    it.bin.freeRects.add(it)
                                }
                        }

                    // merge rectangles
                    placedPart.bin.mergeRectangles(freeRects)

                    // remove contained rects
                    placedPart.bin.freeRects
                        .combine(placedPart.bin.freeRects)
                        .filter { it.first != it.second && it.first.rect.contains(it.second.rect) }
                        .map { it.second }
                        .forEach {
                            freeRects.remove(it)
                            it.bin.freeRects.remove(it)
                        }
                }

                if (copyBinPattern) {
                    // copy the pattern if the bin is full
                    if (placedPart.bin.isFull(availablePartDemands)) {
                        //println(partPlaced.bin.partsPlaced.size)
                        //println(partPlaced.bin.freeRects)
                        //println(availablePartDemands)
                        copyBinPattern(placedPart.bin)
                    }
                }
            }
        }

        return if (availablePartDemands.isEmpty()) {

            var bins: List<Bin> = listOf()
            val messages: MutableList<String> = mutableListOf()
            val postProcessingTime = measureTimeMillis {
                // close the bins
                for (bin in usedBins) {
                    bin.close()
                }
                // merge equal bins
                bins = distinctBins(usedBins)
                // optimize first level strips grouping

//                val optimizedBins: List<Bin> = bins.groupBy {
//                    Pair(it.master, it.strip.cutConstraint)
//                }.flatMap { entry ->
//                    val master = entry.key.first
//                    val cutConstraint = entry.key.second
//                    val bins = entry.value
//                    val directPlaced = bins.flatMap { it.strip.placedParts }.size
//                    val count = bins.map { it.quantity }.sum()
//                    val elements = bins.flatMap { bin -> (1..bin.quantity).flatMap { bin.strip.cuts.map { it + 4.4 } } }.sorted()
//                    // TODO also reshufle with direct placed elements
//                    if (elements.size > 1 && directPlaced == 0) {
//                        val totalElements = elements.sum()
//                        val binSize = when (cutConstraint) {
//                            CutConstraint.HORIZONTAL -> master.freeRect.height + 4.4
//                            CutConstraint.VERTICAL -> master.freeRect.width + 4.4
//                            else -> throw RuntimeException("unexpected ${cutConstraint}")
//                        }
//                        val lowerBound = ((totalElements / binSize) - 0.01).toInt() + 1
//                        if (lowerBound < count) {
//                            val optimizedBins = pack1d.Solver1D(elements, binSize).best_fit_decreasing()
//                            if (optimizedBins.size < count) {
//                                messages.add("could reduce ${count} to ${optimizedBins.size}")
//                                // entry.value
//                                val strips =
//                                    entry.value
//                                        .flatMap { bin ->
//                                            (1..bin.quantity).flatMap { bin.strip.childs }
//                                        }
//                                        .toMutableList()
//                                optimizedBins.map { bin1D ->
//                                    val optimizedStrips: MutableList<Strip> = mutableListOf()
//                                    for (cut in bin1D.items) {
//                                        val strip = strips.find { strip: Strip ->
//                                            when (strip.parent?.cutConstraint) {
//                                                CutConstraint.HORIZONTAL -> strip.rect.height + 4.4 == cut
//                                                CutConstraint.VERTICAL -> strip.rect.width + 4.4 == cut
//                                                else -> throw RuntimeException("unexpected")
//                                            }
//                                        }
//                                        if (strip != null) {
//                                            strips.remove(strip!!)
//                                            optimizedStrips.add(strip!!)
//                                        } else {
//                                            // println(elements)
//                                            // println("strips: ${strips.map { it.rect }}")
//                                            // println("not found : ${cut}")
//                                             throw RuntimeException("strip not found")
//                                        }
//                                    }
//                                    optimizedStrips.sortBy { strip: Strip ->
//                                        when (strip.parent?.cutConstraint) {
//                                            CutConstraint.HORIZONTAL -> -strip.rect.height
//                                            CutConstraint.VERTICAL -> -strip.rect.width
//                                            else -> throw RuntimeException("unexpected")
//                                        }
//                                    }
//                                    var offset = 0.0
//                                    for (strip in optimizedStrips) {
//                                        when (strip.parent?.cutConstraint) {
//                                            CutConstraint.HORIZONTAL -> {
//                                                val delta = offset - strip.rect.bottom
//                                                strip.rect.bottom += delta
//                                                for (placedPart in strip.allPlacedParts()) { placedPart.bottom += delta  }
//                                                offset += (strip.rect.height + 4.4)
//                                            }
//                                            CutConstraint.VERTICAL -> {
//                                                val delta = offset - strip.rect.left
//                                                for (placedPart in strip.allPlacedParts()) { placedPart.left += delta  }
//                                                offset += (strip.rect.width + 4.4)
//                                            }
//                                            else -> throw RuntimeException("unexpected")
//                                        }
//                                    }
//                                    val bin = master.copy(quantity = 1)
//                                    bin.strip.cutConstraint = cutConstraint
//                                    bin.strip.childs.clear()
//                                    bin.strip.childs.addAll(optimizedStrips)
//                                    bin.strip.cuts.clear()
//                                    bin.strip.cuts.addAll(bin.strip.cuts())
//                                    bin
//                                }
//                                // entry.value
//                            } else
//                                entry.value
//                        } else
//                            entry.value
//                    } else
//                        entry.value
//                }
//                 bins = distinctBins(optimizedBins)
            }
            val solution = Solution(bins, this, time + postProcessingTime)
            solution.remarks.addAll(messages)
            solution
        } else {
            null
        }
    }

    private fun distinctBins(bins: List<Bin>): List<Bin> =
        bins
            .groupBy { it.placedParts().map { it.partDemand } }
            .map {
                val bin = it.value.first()
                val totalQuantity = it.value.map { it.quantity }.sum()
                bin.quantity = totalQuantity
                bin
            }
    

    override fun toString() =
        "global:${globalFreeRectangleSearch}, copy:${copyBinPattern}, ${partSorter}, ${binSorter}, ${partToRectMatcher}, ${freeRectSplitter}, ${variant}"

    val name: String =
        "${globalFreeRectangleSearch}_${copyBinPattern}_${partSorter}_${binSorter}_${partToRectMatcher}_${freeRectSplitter}_${variant}"

    companion object {
        fun sufficientParts(parts: Iterable<PlacedPart>): Boolean =
            // lazy grouping: the groups are actually built right before the operation execution.
            parts.groupingBy { it.partDemand }
                .eachCount()
                .all { (master, count) -> master.quantity >= count }

    }

}