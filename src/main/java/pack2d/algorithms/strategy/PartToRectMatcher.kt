package pack2d.algorithms.strategy

import pack2d.common.Rect
import kotlin.math.max
import kotlin.math.min

// rect selection
enum class PartToRectMatcher {
    BestShortestSideFit {
        override fun fitness(part: Rect, freeRect: Rect): Double =
            min(freeRect.width - part.width, freeRect.height - part.height)
    },
    WorstShortestSideFit {
        override fun fitness(part: Rect, freeRect: Rect): Double =
            -min(freeRect.width - part.width, freeRect.height - part.height)
    },
    BestLongestSideFit {
        override fun fitness(part: Rect, freeRect: Rect): Double =
            max(freeRect.width - part.width, freeRect.height - part.height)
    },
    WorstLongestSideFit {
        override fun fitness(part: Rect, freeRect: Rect): Double =
            -max(freeRect.width - part.width, freeRect.height - part.height)
    },
    BestSurfaceFit {
        override fun fitness(part: Rect, freeRect: Rect): Double =
            freeRect.surface - part.surface
    },
    WorstSurfaceFit {
        override fun fitness(part: Rect, freeRect: Rect): Double =
            -(freeRect.surface - part.surface)
    };

    abstract fun fitness(part: Rect, freeRect: Rect): Double

    companion object {
        val veryBest = listOf(
            BestShortestSideFit
        )

        val best = listOf(
            BestShortestSideFit,
            BestLongestSideFit,
            BestSurfaceFit
        )

        val all = listOf(
            BestShortestSideFit,
            BestLongestSideFit,
            BestSurfaceFit,
            WorstShortestSideFit,
            WorstLongestSideFit,
            WorstSurfaceFit
        )

        val nesting = listOf(
            BestShortestSideFit
        )
    }
}
