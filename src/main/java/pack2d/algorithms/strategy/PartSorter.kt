package pack2d.algorithms.strategy

import pack2d.common.PartDemand

enum class PartSorter {
    DescendingShorterSide {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareByDescending<PartDemand> { it.minDim }.thenByDescending<PartDemand> { it.maxDim })
                .toMutableList()
    },
    AscendingShorterSide {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareBy<PartDemand> { it.minDim }.thenBy<PartDemand> { it.maxDim })
                .toMutableList()
    },
    DescendingLongestSide {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareByDescending<PartDemand> { it.maxDim }.thenByDescending<PartDemand> { it.minDim })
                .toMutableList()
    },
    AscendingLongestSide {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareBy<PartDemand> { it.maxDim }.thenBy<PartDemand> { it.minDim })
                .toMutableList()
    },
    DescendingSurface {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareByDescending<PartDemand> { it.surface }.thenByDescending<PartDemand> { it.minDim })
                .toMutableList()
    },
    AscendingSurface {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareBy<PartDemand> { it.surface }.thenByDescending<PartDemand> { it.minDim })
                .toMutableList()
    },
    AscendingRatio {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareBy<PartDemand> { it.minDim / it.maxDim }.thenBy<PartDemand> { it.surface })
                .toMutableList()
    },
    DescendingRatio {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareByDescending<PartDemand> { it.minDim / it.maxDim }.thenBy<PartDemand> { it.surface })
                .toMutableList()
    },
    AscendingDiff {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareBy<PartDemand> { it.maxDim - it.minDim }.thenBy<PartDemand> { it.surface })
                .toMutableList()
    },
    DescendingDiff {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareByDescending<PartDemand> { it.maxDim - it.minDim }.thenBy<PartDemand> { it.surface })
                .toMutableList()
    },
    DescendingPerimeter {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareByDescending<PartDemand> { it.width * 2 + it.height * 2 }.thenBy<PartDemand> { it.surface })
                .toMutableList()
    },
    AscendingPerimeter {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .sortedWith(compareBy<PartDemand> { it.width * 2 + it.height * 2 }.thenBy<PartDemand> { it.surface })
                .toMutableList()
    },
    Shuffle {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .shuffled()
                .toMutableList()
    },
    None {
        override fun sort(parts: List<PartDemand>): MutableList<PartDemand> =
            parts
                .map { it.copy().normalized() }
                .toMutableList()
    };

    abstract fun sort(parts: List<PartDemand>): MutableList<PartDemand>

    companion object {
        val veryBest = listOf(
            DescendingShorterSide
        )
        val best = listOf(
            DescendingShorterSide,
            DescendingSurface,
            DescendingLongestSide,
            DescendingPerimeter
        )
        val all = listOf(
            DescendingShorterSide,
            AscendingShorterSide,
            DescendingSurface,
            AscendingSurface,
            DescendingLongestSide,
            AscendingLongestSide,
            DescendingPerimeter,
            AscendingPerimeter,
            DescendingRatio,
            AscendingRatio,
            DescendingDiff,
            AscendingDiff
        )
    }
}