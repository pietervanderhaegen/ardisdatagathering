package pack2d.algorithms.strategy

import pack2d.common.CutConstraint
import pack2d.common.FreeRect
import pack2d.common.PlacedPart

// free rect splitting
enum class FreeRectSplitter {
    NestingSplit {
        override fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect> {
            val fvs = freeRect.verticalCut(placedPart)
            val fhs = freeRect.horizontalCut(placedPart)
            return fvs + fhs
        }
    },
    ShortestAxisSplit {
        // SAS shortest axis split
        override fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect> =
            if (freeRect.rect.width < freeRect.rect.height)
                freeRect.verticalCut(placedPart)
            else
                freeRect.horizontalCut(placedPart)
    },
    LongestAxisSplit {
        // LAS longest axis split
        override fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect> =
            if (freeRect.rect.width > freeRect.rect.height)
                freeRect.verticalCut(placedPart)
            else
                freeRect.horizontalCut(placedPart)
    },
    ShortestLeftoverAxisSplit {
        // SLAS shortest leftover axis split
        override fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect> =
            if (freeRect.rect.width - placedPart.width < freeRect.rect.height - placedPart.height)
                freeRect.verticalCut(placedPart)
            else
                freeRect.horizontalCut(placedPart)
    },
    LongestLeftoverAxisSplit {
        // LLAS longest leftover axis split
        override fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect> =
            if (freeRect.rect.width - placedPart.width > freeRect.rect.height - placedPart.height)
                freeRect.verticalCut(placedPart)
            else
                freeRect.horizontalCut(placedPart)
    },
    ShortestAxisSplitStrip {
        override fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect> =
            when (freeRect.strip!!.cutConstraint) {
                CutConstraint.FREE -> if (freeRect.rect.width < freeRect.rect.height)
                    freeRect.verticalCut(placedPart)
                else
                    freeRect.horizontalCut(placedPart)
                CutConstraint.HORIZONTAL -> freeRect.horizontalCut(placedPart)
                CutConstraint.VERTICAL -> freeRect.verticalCut(placedPart)
            }
    },
    LongestAxisSplitStrip {
        override fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect> =
            when (freeRect.strip!!.cutConstraint) {
                CutConstraint.FREE -> if (freeRect.rect.width > freeRect.rect.height)
                    freeRect.verticalCut(placedPart)
                else
                    freeRect.horizontalCut(placedPart)
                CutConstraint.HORIZONTAL -> freeRect.horizontalCut(placedPart)
                CutConstraint.VERTICAL -> freeRect.verticalCut(placedPart)
            }
    },
    ShortestLeftOverAxisSplitStrip {
        override fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect> =
            when (freeRect.strip!!.cutConstraint) {
                CutConstraint.FREE -> if (freeRect.rect.width - placedPart.width < freeRect.rect.height - placedPart.height)
                    freeRect.verticalCut(placedPart)
                else
                    freeRect.horizontalCut(placedPart)
                CutConstraint.HORIZONTAL -> freeRect.horizontalCut(placedPart)
                CutConstraint.VERTICAL -> freeRect.verticalCut(placedPart)
            }
    },
    LongestLeftOverAxisSplitStrip {
        override fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect> =
            when (freeRect.strip!!.cutConstraint) {
                CutConstraint.FREE -> if (freeRect.rect.width - placedPart.width > freeRect.rect.height - placedPart.height)
                    freeRect.verticalCut(placedPart)
                else
                    freeRect.horizontalCut(placedPart)
                CutConstraint.HORIZONTAL -> freeRect.horizontalCut(placedPart)
                CutConstraint.VERTICAL -> freeRect.verticalCut(placedPart)
            }
    },
    FlexStrip {
        override fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect> =
            when (freeRect.strip!!.cutConstraint) {
                CutConstraint.FREE ->
                    freeRect.verticalCut(placedPart) +
                    freeRect.horizontalCut(placedPart)
                CutConstraint.HORIZONTAL -> freeRect.horizontalCut(placedPart)
                CutConstraint.VERTICAL -> freeRect.verticalCut(placedPart)
            }
    };

    // (FreeRect, PartPlaced) -> List<FreeRect>
    abstract fun split(freeRect: FreeRect, placedPart: PlacedPart): List<FreeRect>

    companion object {
        val all = listOf<FreeRectSplitter>(
            ShortestAxisSplit,
            LongestAxisSplit,
            ShortestLeftoverAxisSplit,
            LongestLeftoverAxisSplit,
            ShortestAxisSplitStrip,
            LongestAxisSplitStrip,
            ShortestLeftOverAxisSplitStrip,
            LongestLeftOverAxisSplitStrip,
            NestingSplit
        )

        val nesting = listOf<FreeRectSplitter>(
            NestingSplit
        )

        val pure = listOf<FreeRectSplitter>(
            ShortestAxisSplit,
            LongestAxisSplit,
            ShortestLeftoverAxisSplit,
            LongestLeftoverAxisSplit
        )

        val strip = listOf<FreeRectSplitter>(
            ShortestAxisSplitStrip,
            LongestAxisSplitStrip,
            ShortestLeftOverAxisSplitStrip,
            LongestLeftOverAxisSplitStrip
        )

        val stripBest = listOf<FreeRectSplitter>(
            ShortestAxisSplitStrip,
            ShortestLeftOverAxisSplitStrip
        )

//        val flexStrip = listOf<FreeRectSplitter>(
//            FlexStrip
//        )
    }
}
