package pack2d.algorithms.strategy

import pack2d.common.Bin

enum class BinSorter {
    DescendingShorterSide {
        // descending shorter side
        override fun sort(bins: List<Bin>): MutableList<Bin> =
            bins
                .map { it.normalizedCopy() }
                .sortedWith(compareByDescending<Bin> { it.minDim }.thenByDescending<Bin> { it.maxDim })
                .toMutableList()
    },
    AscendingShorterSide {
        override fun sort(bins: List<Bin>): MutableList<Bin> =
            bins
                .map { it.normalizedCopy() }
                .sortedWith(compareBy<Bin> { it.minDim }.thenBy<Bin> { it.maxDim })
                .toMutableList()
    },
    DescendingSurface {
        override fun sort(bins: List<Bin>): MutableList<Bin> =
            bins
                .map { it.normalizedCopy() }
                .sortedByDescending { it.surface }
                .toMutableList()
    },
    AscendingSurface {
        override fun sort(bins: List<Bin>): MutableList<Bin> =
            bins
                .map { it.normalizedCopy() }
                .sortedBy { it.surface }
                .toMutableList()
    };

    abstract fun sort(bins: List<Bin>): MutableList<Bin>

    companion object {
        val best = listOf(
            DescendingSurface
        )

        val all = listOf(
            DescendingShorterSide,
            AscendingShorterSide,
            DescendingSurface,
            AscendingSurface
        )
    }
}