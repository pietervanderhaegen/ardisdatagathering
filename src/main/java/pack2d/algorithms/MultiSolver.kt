package pack2d.algorithms

import pack2d.algorithms.strategy.BinSorter
import pack2d.algorithms.strategy.FreeRectSplitter
import pack2d.algorithms.strategy.PartSorter
import pack2d.algorithms.strategy.PartToRectMatcher
import pack2d.common.Bin
import pack2d.common.PartDemand
import main.java.pack2d.common.Solution
import pack2d.common.Variant

object MultiSolver {

    fun strip(bins: List<Bin>, parts: List<PartDemand>, maxSurface: Double = Double.MAX_VALUE): Solution? =
        solveAll(stripSolvers(bins, parts), maxSurface)

    fun stripSolvers(bins: List<Bin>, parts: List<PartDemand>): List<Solver2D> {
        val actualParts = parts.map { it.quantity }.sum()
        return when (actualParts) {
            in 0..500 ->
                combine(
                    globalSearchValues = listOf(false),
                    copyBinPatternValues = listOf(true, false),
                    binSorters = BinSorter.all,
                    partSorters = PartSorter.all,
                    partToRectMatchers = PartToRectMatcher.all,
                    freeRectSplitters = FreeRectSplitter.strip,
                    variant = Variant.STRIP,
                    bins = bins,
                    partsList = listOf(parts)
                ) + combine(
                    globalSearchValues = listOf(true),
                    copyBinPatternValues = listOf(true, false),
                    binSorters = BinSorter.all,
                    partSorters = listOf(PartSorter.DescendingSurface),
                    partToRectMatchers = PartToRectMatcher.all,
                    freeRectSplitters = FreeRectSplitter.strip,
                    variant = Variant.STRIP,
                    bins = bins,
                    partsList = listOf(parts)
                )
            in 500..1000 ->
                combine(
                    globalSearchValues = listOf(false),
                    copyBinPatternValues = listOf(true, false),
                    binSorters = BinSorter.all,
                    partSorters = PartSorter.all,
                    partToRectMatchers = PartToRectMatcher.best,
                    freeRectSplitters = FreeRectSplitter.strip,
                    variant = Variant.STRIP,
                    bins = bins,
                    partsList = listOf(parts)
                ) + combine(
                    globalSearchValues = listOf(true),
                    copyBinPatternValues = listOf(true, false),
                    binSorters = BinSorter.best,
                    partSorters = listOf(PartSorter.DescendingSurface),
                    partToRectMatchers = PartToRectMatcher.best,
                    freeRectSplitters = FreeRectSplitter.stripBest,
                    variant = Variant.STRIP,
                    bins = bins,
                    partsList = listOf(parts)
                )
            in 1000..3000 ->
                combine(
                    globalSearchValues = listOf(false),
                    copyBinPatternValues = listOf(true),
                    binSorters = BinSorter.best,
                    partSorters = PartSorter.best,
                    partToRectMatchers = PartToRectMatcher.best,
                    freeRectSplitters = FreeRectSplitter.strip,
                    variant = Variant.STRIP,
                    bins = bins,
                    partsList = listOf(parts)
                )
            else ->
                combine(
                    globalSearchValues = listOf(false),
                    copyBinPatternValues = listOf(true),
                    binSorters = BinSorter.best,
                    partSorters = PartSorter.best,
                    partToRectMatchers = PartToRectMatcher.veryBest,
                    freeRectSplitters = FreeRectSplitter.stripBest,
                    variant = Variant.STRIP,
                    bins = bins,
                    partsList = listOf(parts)
                )
        }
    }

    fun flexStrip(bins: List<Bin>, parts: List<PartDemand>, maxSurface: Double = Double.MAX_VALUE): Solution? =
        solveAll(stripSolvers(bins, parts), maxSurface)

//    fun flexStripSolvers(bins: List<Bin>, parts: List<PartDemand>): List<Solver> {
//        return combine(
//            listOf(false),
//            listOf(true, false),
//            BinSorter.all,
//            PartSorter.all,
//            PartToRectMatcher.all,
//            FreeRectSplitter.flexStrip,
//            Variant.STRIP,
//            bins,
//            listOf(parts)
//        ) + combine(
//            listOf(true),
//            listOf(true, false),
//            BinSorter.all,
//            listOf(PartSorter.DescendingSurface),
//            PartToRectMatcher.all,
//            FreeRectSplitter.strip,
//            Variant.STRIP,
//            bins,
//            listOf(parts)
//        )
//    }

    fun guillotine(bins: List<Bin>, parts: List<PartDemand>, maxSurface: Double = Double.MAX_VALUE): Solution? {

        val solvers =
            combine(
                listOf(false),
                listOf(true, false),
                BinSorter.all,
                PartSorter.all,
                PartToRectMatcher.all,
                FreeRectSplitter.pure,
                Variant.GUILLOTINE,
                bins,
                listOf(parts)
            )

        return solveAll(solvers, maxSurface)
    }

    fun nesting(bins: List<Bin>, parts: List<PartDemand>, maxSurface: Double = Double.MAX_VALUE): Solution? {

        val solvers =
            combine(
                listOf(false),
                listOf(true, false),
                BinSorter.all,
                listOf(PartSorter.DescendingSurface),
                PartToRectMatcher.nesting,
                FreeRectSplitter.nesting,
                Variant.NESTING,
                bins,
                listOf(parts)
            ) + combine(
                listOf(false),
                listOf(true, false),
                BinSorter.all,
                PartSorter.all,
                PartToRectMatcher.nesting,
                FreeRectSplitter.nesting,
                Variant.NESTING,
                bins,
                listOf(parts)
            )

        return solveAll(solvers, maxSurface)
    }

    fun shuffledStripSolver(
        bins: List<Bin>,
        parts: List<PartDemand>
    ) =
        Solver2D(
            bins,
            parts,
            false,
            listOf(true, false).shuffled().first(),
            // listOf<Boolean>(false, false, false, false, true).shuffled().first(),
            PartSorter.Shuffle,
            BinSorter.all.shuffled().first(),
            // PartToRectMatcher.BestShortestSideFit,
            PartToRectMatcher.all.shuffled().first(),
            // FreeRectSplitter.ShortestAxisSplitStrip,
            // FreeRectSplitter.FlexStrip,
            FreeRectSplitter.strip.shuffled().first(),
            Variant.STRIP
        )

    private fun shuffledStripSolverList(
        bins: List<Bin>,
        parts: List<PartDemand>,
        count: Int
    ): List<Solver2D> =
        (1..count).map { shuffledStripSolver(bins, parts) }

    fun shuffledStrip(bins: List<Bin>, parts: List<PartDemand>, kCountMax: Int = 1, currentBest: Solution): Solution {
        val start = System.currentTimeMillis()
        val improved = (1..kCountMax).fold(currentBest, { best: Solution, _: Int ->
            val stop = System.currentTimeMillis()
            if (stop - start > 30000) {
                // print("x")
                return best
            }
            val guillotines = shuffledStripSolverList(bins, parts, 10)
            val solution = solveAll(guillotines, Double.MAX_VALUE)
            if (solution != null && solution.cost() < best.cost()) {
                print("+")
                solution
            } else {
                // if (solution?.cost() == best.cost()) print("=") else print("-")
                best
            }
        })
        println("\n")
        return improved
    }

    fun combine(
        globalSearchValues: List<Boolean>,
        copyBinPatternValues: List<Boolean>,
        binSorters: List<BinSorter>,
        partSorters: List<PartSorter>,
        partToRectMatchers: List<PartToRectMatcher>,
        freeRectSplitters: List<FreeRectSplitter>,
        variant: Variant,
        bins: List<Bin>,
        partsList: List<List<PartDemand>>
    ): List<Solver2D> =
        globalSearchValues.flatMap { globalSearch ->
            copyBinPatternValues.flatMap { copyBinPattern ->
                partSorters.flatMap { partSorter ->
                    binSorters.flatMap { binSorter ->
                        partToRectMatchers.flatMap { partToRectMatcher ->
                            freeRectSplitters.flatMap { freeRectSplitter ->
                                partsList.map { parts ->
                                    Solver2D(
                                        bins,
                                        parts,
                                        globalSearch,
                                        copyBinPattern,
                                        partSorter,
                                        binSorter,
                                        partToRectMatcher,
                                        freeRectSplitter,
                                        variant
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }


    fun solveAll(solvers: List<Solver2D>, maxSurface: Double = Double.MAX_VALUE): Solution? =
        solveAllParallel(solvers, maxSurface)
        //solveAllSequential(solvers, maxSurface)


    private fun solveAllSequential(solvers: List<Solver2D>, maxSurface: Double = Double.MAX_VALUE): Solution? {
        return solvers
            .mapNotNull {
                val solution = it.solve(maxSurface)
                // print(".")
                // if (solution != null)
                //    ExportSVG.exportBins("output/_${it.name}.strip.html", solution)
                solution
            }
//            .mapNotNull {
//                println(it.binsSurfacem2())
//                it
//            }
            .minBy { it.cost() }
    }

    // concurrent implementation using java streams
    private fun solveAllParallel(solvers: List<Solver2D>, maxSurface: Double = Double.MAX_VALUE): Solution? =
    // consider coroutines variant
        // https://jivimberg.io/blog/2018/05/04/parallel-map-in-kotlin/
        solvers
            .parallelStream()
            .map {
                val solution = it.solve(maxSurface)
                // print('.')
//                 if (solution != null)
//                    ExportSVG.exportBins("output/_${it.name}.strip.html", solution)
                solution
            }
            .filter { it != null }
//            .map {
//                println("${it?.binsSurfacem2()} ${it?.cost()} ${it!!.solver2D} ${it!!.remarks}")
//                it
//            }
            .min { a, b ->
                when {
                    a == null || b == null -> 0
                    a.cost() >= b.cost() -> 1
                    a.cost() < b.cost() -> -1
                    else -> 0
                }
            }
            .orElseGet { null }

}
