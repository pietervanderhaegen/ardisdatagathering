package pack2d

import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.NodeList
import pack2d.algorithms.MultiSolver
import pack2d.common.Bin
import pack2d.common.Grain
import pack2d.common.PartDemand
import main.java.pack2d.common.Solution
import pack2d.util.ExportSVG
import pack2d.util.round
import java.io.File
import java.io.FileWriter
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants
import javax.xml.xpath.XPathFactory
import kotlin.system.measureTimeMillis

fun readXml(xmlFile: File): Document {
    val dbFactory = DocumentBuilderFactory.newInstance()
    val dBuilder = dbFactory.newDocumentBuilder()
    val doc = dBuilder.parse(xmlFile)
    doc.normalize()
    return doc
}

fun text(elem: Element, tagName: String) =
    elem.getElementsByTagName(tagName).item(0)?.textContent ?: ""

fun partsFromXml(doc: Document): List<PartDemand> {
    val xPath: XPath = XPathFactory.newInstance().newXPath();
    val partsPath = xPath.compile("/ArdisOptimizerProject/PartCollection/PartFile/PartList/Part")

    val partsNodeList: NodeList = partsPath.evaluate(doc, XPathConstants.NODESET) as NodeList;
    return (0 until partsNodeList.length)
        .map {
            val part = partsNodeList.item(it)
            val partElem = part as Element
            val partMat = text(partElem, "PartMat")
            val partL: Double = try {
                text(partElem, "PartL").toDouble()
            } catch (e: NumberFormatException) {
                0.0
            }
            val partW: Double = try {
                text(partElem, "PartW").toDouble()
            } catch (e: NumberFormatException) {
                0.0
            }
            val partQty = try {
                text(partElem, "PartQty").toInt()
            } catch (e: NumberFormatException) {
                0
            }
            val partD = try {
                text(partElem, "PartD").toInt()
            } catch (e: NumberFormatException) {
                0
            }
            val partGrain = Grain.values()[if (partD in 0..2) partD else 0]
            if (partW > 0.0 && partW > 0.0 && partQty > 0)
                List(1) { _ -> PartDemand(partMat, partL, partW, partGrain, partQty) }
            else
                emptyList()
        }
        .flatten()
        .groupBy { it.key() }
        .map {
            val quantity = it.value.map { it.quantity }.sum()
            it.key.partDemand(quantity)
        }
}

fun Solution.whoByGross(resGross: Double) =
    when {
        binsSurfacem2().round(0) < resGross.round(0) -> "M"
        binsSurfacem2().round(0) > resGross.round(0) -> "A"
        else -> "D"
    }

fun Solution.whoByCost(resCost: Double) =
    when {
        cost().round(0) < resCost.round(0) -> "M"
        cost().round(0) > resCost.round(0) -> "A"
        else -> "D"
    }

fun binsFromXml(doc: Document): List<Bin> {
    val xPath: XPath = XPathFactory.newInstance().newXPath();
    val sheetsPath = xPath.compile("/ArdisOptimizerProject/SheetCollection/SheetFile/SheetList/Sheet")

    val sheetsNodeList: NodeList = sheetsPath.evaluate(doc, XPathConstants.NODESET) as NodeList;
    return (0 until sheetsNodeList.length)
        .mapNotNull { index ->
            val sheet = sheetsNodeList.item(index)
            val sheetElem = sheet as Element
            val sheetMat = text(sheetElem, "SheetMat")
            val sheetL = try {
                text(sheetElem, "SheetL").toDouble()
            } catch (e: NumberFormatException) {
                0.0
            }
            val sheetW = try {
                text(sheetElem, "SheetW").toDouble()
            } catch (e: NumberFormatException) {
                0.0
            }
            val sheetNoLimit = try {
                text(sheetElem, "SheetNoLimit").toDouble().toInt()
            } catch (e: Exception) {
                -1
            }
            val sheetQty = try {
                Regex("[^0-9]").replace(text(sheetElem, "SheetQty"), "").toInt()
            } catch (e: NumberFormatException) {
                if (text(sheetElem, "SheetQty") == "" && sheetNoLimit ==1)
                    Int.MAX_VALUE
                else {
                    println(e)
                    Int.MAX_VALUE
                }
            }
            val sheetRestmL = try {
                text(sheetElem, "SheetRestmL").toDouble()
            } catch (e: NumberFormatException) {
                0.0
            }
            val sheetRestmW = try {
                text(sheetElem, "SheetRestmW").toDouble()
            } catch (e: NumberFormatException) {
                0.0
            }
            val sheetRestmSurf = try {
                text(sheetElem, "SheetRestmSurf").toDouble()
            } catch (e: NumberFormatException) {
                0.0
            }
            val sheetPrice = try {
                text(sheetElem, "SheetPrice").toDouble()
            } catch (e: NumberFormatException) {
                0.0
            }
            val sheetD = try {
                text(sheetElem, "SheetD").toInt()
            } catch (e: NumberFormatException) {
                0
            }
            val sheetThick = try {
                text(sheetElem, "SheetThick").toDouble()
            } catch (e: NumberFormatException) {
                18.0
            }
            ///
            val actualSheetQty: Int = when {
                (sheetNoLimit == 1) -> Int.MAX_VALUE
                (sheetNoLimit == 2) -> if (sheetQty > 0) sheetQty else 1
                (sheetNoLimit == 3) -> 0
                else -> sheetQty
            }
            if(actualSheetQty<0){
                print("error ja ")
            }

            val isOffcut: Boolean = sheetNoLimit == 2
            // TODO case for 3
            val sheetGrain = Grain.values()[if (sheetD >= 0 && sheetD <= 2) sheetD else 0]
            ///
            if (sheetL > 0.0 && sheetW > 0.0 && sheetThick >= 0.0 && actualSheetQty > 0) {
                Bin(
                    sheetMat,
                    sheetL,
                    sheetW,
                    sheetThick,
                    sheetGrain,
                    actualSheetQty,
                    isOffcut,
                    sheetRestmL,
                    sheetRestmW,
                    sheetRestmSurf,
                    sheetPrice
                )
            } else {
                null
            }
        }
}

fun resultFromXml(doc: Document): Map<String, Double> {
    val xPath: XPath = XPathFactory.newInstance().newXPath();
    val sheetsPath = xPath.compile("/ArdisOptimizerProject/Result/ResultProperties")
    val resultPropertiesList: NodeList = sheetsPath.evaluate(doc, XPathConstants.NODESET) as NodeList;

    val resultProperties = resultPropertiesList.item(0)
    val sheetElem = resultProperties as Element
    val resCost = try {
        text(sheetElem, "ResRemark4").toDouble()
    } catch (e: NumberFormatException) {
        0.0
    }
    val resNofSheets = try {
        text(sheetElem, "ResNofSheets").toDouble()
    } catch (e: NumberFormatException) {
        0.0
    }
    val resGross = try {
        text(sheetElem, "ResGross").toDouble()
    } catch (e: NumberFormatException) {
        0.0
    }
    val resNet = try {
        text(sheetElem, "ResNet").toDouble()
    } catch (e: NumberFormatException) {
        0.0
    }
    return mapOf(
        "ResCost" to resCost,
        "ResNofSheets" to resNofSheets,
        "ResGross" to resGross,
        "ResNet" to resNet
    )
}

fun proccesFile(file: File, report: FileWriter) {
    // read do
    val doc = readXml(file)
    // parse sections
    val parts = partsFromXml(doc)
    val bins = binsFromXml(doc)
    val result = resultFromXml(doc)
    // extract result details
    val resCost = result["ResCost"] ?: 0.0
    val resNofSheets = result["ResNofSheets"] ?: 0.0
    val resGross = result["ResGross"] ?: 0.0
    val resNet = result["ResNet"] ?: 0.0
    val resYield: Double = result["ResYield"] ?: 0.0
    // group
    val partsPerMaterial = parts.groupBy { it.material }
    val relevantMaterial = partsPerMaterial.keys
    val binsPerMaterial = bins.filter { relevantMaterial.contains(it.material) }.groupBy { it.material }
    val actualParts = partsPerMaterial.values.flatMap { it.map { it.quantity } }.sum()
    // ready
    println("ready: ${file.name} with ${parts.size} distinct part sizes and ${bins.size} bins for ${actualParts} actual parts")
    // we currently assume there is only one material
    if (partsPerMaterial.size == 1 && resGross > 0.1) {
        // the material, parts and bins
        val material = partsPerMaterial.entries.first().key
        val partsForMaterial = partsPerMaterial.entries.first().value
        val binsForMaterial = binsPerMaterial[material] ?: listOf()
        val firstSolution = MultiSolver.strip(binsForMaterial, partsForMaterial, Double.MAX_VALUE)
        if (firstSolution != null) {
            val solution =
//                if (actualParts < 200)
//                    MultiSolver.shuffledStrip(bins, partsForMaterial, 10000, firstSolution)
//                else
                    firstSolution
            println("${solution.whoByGross(resGross)}/${solution.whoByCost(resCost)} <- M:${solution.binsSurfacem2().round(2)} <> A:${resGross.round(2)}, M:${solution.cost().round(2)} <> A:${resCost.round(2)}} ${file.name}")
            if (solution.remarks.isNotEmpty())
                println(solution.remarks)
            report.append(
                "${file.name}, ${solution.whoByGross(resGross)}, ${solution.binsSurfacem2().round(2)}, ${resGross.round(2)}, ${solution.whoByCost(resCost)}, ${solution.cost().round(2)}, ${resCost.round(2)}, ${solution.yield().round(2)}, ${(resNet/resGross).round(2)}, ${solution.partsSurfacem2().round(4)}, ${resNet.round(4)}, ${solution.cost().round(2)}, ${resCost.round(2)}, ${solution.time}, ${solution.solver2D}\n"
            )
            ExportSVG.exportBins("${file.parent}/${file.nameWithoutExtension}.strip.html", solution)
        }
    } else
        println("empty problem for ${file.name}")
}

class Config {
    companion object {
    }
}

fun processFolder(folder: String) {
    val processingTime = measureTimeMillis {
        var xmlCounter = 0
        val report = FileWriter("report.csv")
        report.append("name, pack2d.whoByGross, M:ResGross, A:ResGross, pack2d.whoByCost, M:ResCost, A:ResCost, M:Yld, A:Yld, M:ResNet, A: ResNet, M:Cost, A:Cost, M:Time(ms), M:Global, M:CopyBP, M:PartSorter, M:BinSorter, M:RectToBinMatcher, M:Splitter, M:Variant\n")
        File(folder).walk().forEachIndexed { index, file ->
            if (file.name.matches(Regex(".*\\.xml$", RegexOption.IGNORE_CASE))) {
                println("#${++xmlCounter}")
                proccesFile(file, report)
            }
        }
        report.flush()
        report.close()
    }
    println("total processing time: ${processingTime}")
}