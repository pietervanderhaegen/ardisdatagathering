import java.io.File
import kotlin.system.measureTimeMillis

fun processFolder(folder: String) {
    val processingTime = measureTimeMillis {
        printPartsToCsv("$folder")
        printBinsToCsv("$folder")
        //printResultsToCsv("$folder")
    }
    println("total processing time: $processingTime")
}

fun renameFilesFolder(folder: String) {
    val processingTime = measureTimeMillis {
        var xmlCounter = 0
        File(folder).walk().forEachIndexed { index, file ->
            if (file.name.contains(Regex("^demo_", RegexOption.IGNORE_CASE))) {
                file.renameTo(File(folder + "\\" + file.name.substring(5)))
                xmlCounter++
                println("" + xmlCounter + " " + file.name)
            }
        }

    }
    println("total processing time: $processingTime")
}



