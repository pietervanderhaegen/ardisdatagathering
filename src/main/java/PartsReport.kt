
import org.nield.kotlinstatistics.*
import pack2d.common.Bin
import pack2d.common.PartDemand
import pack2d.partsFromXml
import pack2d.readXml
import java.io.File
import java.io.FileWriter
import kotlin.math.abs

fun proccesPartsFile(file: File, report: FileWriter) {
    // read do
    val doc = readXml(file)

    // parse sections
    val parts = partsFromXml(doc)

    //PART INFO
    val totalParts = getTotalAmountOfParts(parts)
    val partVariation = parts.size.toDouble() /  totalParts.toString().toDouble()

    val minDimDescriptives = parts.map { it.minDim }.descriptiveStatistics
    val maxDimDescriptives = parts.map { it.maxDim }.descriptiveStatistics
    val surfaceDescriptives = parts.map { it.surface }.descriptiveStatistics

    report.append(
        "${file.name}, " +
                "${parts.size}, " +
                "${totalParts}, " +
                "${partVariation}, " +

                "${minDimDescriptives.min}, " +
                "${minDimDescriptives.max}, " +
                "${minDimDescriptives.min / minDimDescriptives.max}, " +
                "${minDimDescriptives.sum}, " +
                "${minDimDescriptives.mean}, " +
                "${parts.map { it.minDim }.median()}, " +
                "${minDimDescriptives.max - minDimDescriptives.min}, " +
                "${minDimDescriptives.variance}, " +
                "${minDimDescriptives.standardDeviation}, " +
                "${minDimDescriptives.geometricMean}, " +
                "${minDimDescriptives.sumSquared}, " +
                "${minDimDescriptives.kurtosis}, " +
                "${minDimDescriptives.skewness}," +

                "${maxDimDescriptives.min}, " +
                "${maxDimDescriptives.max}, " +
                "${maxDimDescriptives.min / minDimDescriptives.max}, " +
                "${maxDimDescriptives.sum}, " +
                "${maxDimDescriptives.mean}, " +
                "${parts.map { it.maxDim }.median()}, " +
                "${maxDimDescriptives.max - minDimDescriptives.min}, " +
                "${maxDimDescriptives.variance}, " +
                "${maxDimDescriptives.standardDeviation}, " +
                "${maxDimDescriptives.geometricMean}, " +
                "${maxDimDescriptives.sumSquared}, " +
                "${maxDimDescriptives.kurtosis}, " +
                "${maxDimDescriptives.skewness}," +

                "${surfaceDescriptives.min}, " +
                "${surfaceDescriptives.max}, " +
                "${surfaceDescriptives.min / surfaceDescriptives.max}, " +
                "${surfaceDescriptives.sum}, " +
                "${surfaceDescriptives.mean}, " +
                "${parts.map { it.surface }.median()}, " +
                "${surfaceDescriptives.max - surfaceDescriptives.min}, " +
                "${surfaceDescriptives.variance}, " +
                "${surfaceDescriptives.standardDeviation}, " +
                "${surfaceDescriptives.geometricMean}, " +
                "${surfaceDescriptives.sumSquared}, " +
                "${surfaceDescriptives.kurtosis}, " +
                "${surfaceDescriptives.skewness}," +


                "${minDimDescriptives.min/maxDimDescriptives.min}," +
                "${minDimDescriptives.max/maxDimDescriptives.max}," +
                "${maxDimDescriptives.min - minDimDescriptives.min}," +
                "${maxDimDescriptives.max - minDimDescriptives.max}" +

                "\n "
    );

}

fun getTotalAmountOfParts(parts: List<PartDemand>): Any {
    val it = parts.iterator()
    var sum = 0;
    while (it.hasNext()) {
        sum += it.next().quantity;
    }
    return sum;
}

fun printPartsToCsv(folder: String) {
    var xmlCounter = 0
    val report = FileWriter("$folder/reports/parts_report.csv")
    report.append(
        "Problem, " +
                "NumberOfDifferentParts, " +
                "TotalAmountOfParts, " +
                "PartVariation, " +

                "MinMinDimParts, " +
                "MaxMinDimParts, " +
                "RatioMinMaxMinDimParts, " +
                "MinDimSumParts, " +
                "AverageMinDimParts, " +
                "MedianMinDimParts, " +
                "RangeMinDimParts, " +
                "VarianceMinDimParts, " +
                "StandardDeviationMinDimParts, " +
                "GeometricMeanMinDimParts, " +
                "SumOfSquaresMinDimParts, " +
                "KurtosisMinDimParts, " +
                "SkewnessMinDimParts, " +

                "MinMaxDimParts, " +
                "MaxMaxDimParts, " +
                "RatioMinMaxMaxDimParts, " +
                "MaxDimSumParts, " +
                "AverageMaxDimParts, " +
                "MedianMaxDimParts, " +
                "RangeMaxDimParts, " +
                "VarianceMaxDimParts, " +
                "StandardDeviationMaxDimParts, " +
                "GeometricMeanMaxDimParts, " +
                "SumOfSquaresMaxDimParts, " +
                "KurtosisMaxDimParts, " +
                "SkewnessMaxDimParts," +

                "MinSurfaceParts, " +
                "MaxSurfaceParts, " +
                "RatioMinMaxSurfaceParts, " +
                "SurfaceSumParts, " +
                "AverageSurfaceParts, " +
                "MedianSurfaceParts, " +
                "RangeSurfaceParts, " +
                "VarianceSurfaceParts, " +
                "StandardDeviationSurfaceParts, " +
                "GeometricMeanSurfaceParts, " +
                "SumOfSquaresSurfaceParts, " +
                "KurtosisSurfaceParts, " +
                "SkewnessSurfaceParts," +

                "minPartRatio," +
                "maxPartRatio," +
                "minPartRange," +
                "maxPartRange" +
                "\n"
    )
    File(folder).walk().forEachIndexed { index, file ->
        if (file.name.matches(Regex(".*\\.xml$", RegexOption.IGNORE_CASE))) {
            proccesPartsFile(file, report)
        }
    }
    report.flush()
    report.close()
}