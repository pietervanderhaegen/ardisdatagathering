import main.java.pack2d.common.Solution
import pack2d.algorithms.MultiSolver
import pack2d.algorithms.Solver2D
import pack2d.algorithms.strategy.BinSorter
import pack2d.algorithms.strategy.FreeRectSplitter
import pack2d.algorithms.strategy.PartSorter
import pack2d.algorithms.strategy.PartToRectMatcher
import pack2d.binsFromXml
import pack2d.common.Bin
import pack2d.common.PartDemand
import pack2d.common.Variant
import pack2d.partsFromXml
import pack2d.readXml
import java.io.File
import java.io.FileWriter
import java.lang.Exception
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.system.measureTimeMillis

fun solveProblemInstances(file: File, report: FileWriter) {
    // read do
    val doc = readXml(file)
    // parse sections
    val parts = partsFromXml(doc)
    val bins = binsFromXml(doc)

    val problemInstances = generateProblems(bins, parts)

    for (problemInstance in problemInstances) {
        try {
            val instanceSolution = problemInstance.solve(Double.MAX_VALUE)
            report.append(
                "${file.name}, " +
                        "${problemInstance.parts.size}, " +
                        "${problemInstance.bins.size}, " +
                        "${problemInstance.globalFreeRectangleSearch}, " +
                        "${problemInstance.copyBinPattern}, " +
                        "${problemInstance.partSorter}, " +
                        "${problemInstance.binSorter}, " +
                        "${problemInstance.freeRectSplitter}, " +
                        "${problemInstance.partToRectMatcher}, " +
                        "${problemInstance.variant}, " +
                        "${instanceSolution?.binsSurfacem2()}, " +
                        "${instanceSolution?.partsSurfacem2()}, " +
                        "${instanceSolution?.yield()}, " +
                        "${instanceSolution?.time}, " +
                        "${instanceSolution?.binsCount()}, " +
                        "${instanceSolution?.cost()} " +
                        "\n "
            )
        } catch (e: OutOfMemoryError) {
            report.append(
                "${file.name}, " +
                        "${problemInstance.parts.size}, " +
                        "${problemInstance.bins.size}, " +
                        "${problemInstance.globalFreeRectangleSearch}, " +
                        "${problemInstance.copyBinPattern}, " +
                        "${problemInstance.partSorter}, " +
                        "${problemInstance.binSorter}, " +
                        "${problemInstance.freeRectSplitter}, " +
                        "${problemInstance.partToRectMatcher}, " +
                        "${problemInstance.variant}, " +
                        "${0}, " +
                        "${0}, " +
                        "${0}, " +
                        "${0}, " +
                        "${0}, " +
                        "${0} " +
                        "\n "
            )
        }
        println("Problem ${problemInstances.indexOf(problemInstance)}: ${file.name}")

    }
}

//GENERATE ALL DIFFERENT PROBLEM INSTANCE POSSIBILITIES
fun generateProblems(bins: List<Bin>, parts: List<PartDemand>): List<Solver2D> {
    return MultiSolver.combine(
        globalSearchValues = listOf(true, false),
        copyBinPatternValues = listOf(true, false),
        binSorters = BinSorter.all,
        partSorters = PartSorter.all,
        partToRectMatchers = PartToRectMatcher.all,
        freeRectSplitters = FreeRectSplitter.strip,
        variant = Variant.STRIP,
        bins = bins,
        partsList = listOf(parts)
    )
}

fun printResultsToCsv(folder: String) {
    val processingTime = measureTimeMillis {
        var xmlCounter = 0
        File(folder).walk().forEachIndexed { index, file ->
            if (file.name.matches(Regex(".*\\.xml$", RegexOption.IGNORE_CASE))) {
                println("#${++xmlCounter}")

                if (!File("$folder/reports/result_reports/${file.name}.csv").exists()){
                val report = FileWriter("$folder/reports/result_reports/${file.name}.csv")
                    report.append(
                        "Problem, " +
                                "Parts, " +
                                "Bins, " +
                                "Global, " +
                                "CopyBinPattern, " +
                                "PartSorter, " +
                                "BinSorter, " +
                                "FreeRectSplitter, " +
                                "PartToRectMatcher, " +
                                "Variant, " +
                                "ResGross, " +
                                "ResNet, " +
                                "Yield, " +
                                "Time, " +
                                "UsedBins, " +
                                "Cost " +
                                "\n"
                    )
                    solveProblemInstances(file, report)
                    report.flush()
                    report.close()
                }
                println("---------------------------------------------------------------------------------------------------------------------------")
            }
        }

    }
    println(message = "total processing time: $processingTime")
}
